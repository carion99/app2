<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190602034109 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE root (id INT AUTO_INCREMENT NOT NULL, code_root VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_register DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE compte_assureur (id INT AUTO_INCREMENT NOT NULL, code_assureur VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, statut_juridique VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone_gsm INT NOT NULL, phone_cel INT NOT NULL, phone_fax INT NOT NULL, adresse VARCHAR(255) NOT NULL, nor_commerce VARCHAR(255) NOT NULL, code_root VARCHAR(255) NOT NULL, permit_root VARCHAR(255) NOT NULL, date_register DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_connect (id INT AUTO_INCREMENT NOT NULL, code_user VARCHAR(255) NOT NULL, date_connect DATETIME NOT NULL, time_connect DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prospect (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, date_register DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contrat_assureur (id INT AUTO_INCREMENT NOT NULL, code_contrat_assureur VARCHAR(255) NOT NULL, code_new_contrat VARCHAR(255) NOT NULL, code_assureur VARCHAR(255) NOT NULL, signature_root VARCHAR(255) NOT NULL, signature_assureur VARCHAR(255) NOT NULL, somme DOUBLE PRECISION NOT NULL, date_register DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, code_user VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, role VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, date_register DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stat_assureur (id INT AUTO_INCREMENT NOT NULL, code_stat_assureur VARCHAR(255) NOT NULL, code_assureur VARCHAR(255) NOT NULL, nbre_agent INT NOT NULL, nbre_client INT NOT NULL, nbre_agent_connect INT NOT NULL, nbre_client_connect INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE relation_agent_client (id INT AUTO_INCREMENT NOT NULL, code_relation VARCHAR(255) NOT NULL, code_agent VARCHAR(255) NOT NULL, code_client VARCHAR(255) NOT NULL, date_register DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE compte_client (id INT AUTO_INCREMENT NOT NULL, code_client VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone INT NOT NULL, profession VARCHAR(255) NOT NULL, date_register DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE model_contrat (id INT AUTO_INCREMENT NOT NULL, code_model_contrat VARCHAR(255) NOT NULL, intitule VARCHAR(255) NOT NULL, nb_client DOUBLE PRECISION NOT NULL, nb_agent DOUBLE PRECISION NOT NULL, version_live_chat VARCHAR(255) NOT NULL, capacite_file_box DOUBLE PRECISION NOT NULL, permit_chat_bot INT NOT NULL, permit_chat_video INT NOT NULL, permit_file_share INT NOT NULL, permit_assistance INT NOT NULL, price_ht DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE new_contrat (id INT AUTO_INCREMENT NOT NULL, code_new_contrat VARCHAR(255) NOT NULL, intitule VARCHAR(255) NOT NULL, code_model_contrat VARCHAR(255) NOT NULL, facteur DOUBLE PRECISION NOT NULL, tva DOUBLE PRECISION NOT NULL, domaine VARCHAR(255) NOT NULL, duree DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE admin_assureur (id INT AUTO_INCREMENT NOT NULL, code_admin_assureur VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, phone INT NOT NULL, adresse VARCHAR(255) NOT NULL, no_cni INT NOT NULL, code_assureur VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE avis_client (id INT AUTO_INCREMENT NOT NULL, code_avis_client VARCHAR(255) NOT NULL, code_client VARCHAR(255) NOT NULL, avis LONGTEXT NOT NULL, date_register DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE avis_assureur (id INT AUTO_INCREMENT NOT NULL, code_avis_assureur VARCHAR(255) NOT NULL, code_asureur VARCHAR(255) NOT NULL, avis LONGTEXT NOT NULL, date_register DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE agent_assureur (id INT AUTO_INCREMENT NOT NULL, code_assureur VARCHAR(255) NOT NULL, code_agent_assureur VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, im_assureur VARCHAR(255) NOT NULL, role VARCHAR(255) NOT NULL, date_register DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE offre (id INT AUTO_INCREMENT NOT NULL, code_offre VARCHAR(255) NOT NULL, code_assureur VARCHAR(255) NOT NULL, intitule VARCHAR(255) NOT NULL, cotisation DOUBLE PRECISION NOT NULL, avantages VARCHAR(255) NOT NULL, domaine VARCHAR(255) NOT NULL, date_register DATETIME NOT NULL, duree DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE root');
        $this->addSql('DROP TABLE compte_assureur');
        $this->addSql('DROP TABLE user_connect');
        $this->addSql('DROP TABLE prospect');
        $this->addSql('DROP TABLE contrat_assureur');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE stat_assureur');
        $this->addSql('DROP TABLE relation_agent_client');
        $this->addSql('DROP TABLE compte_client');
        $this->addSql('DROP TABLE model_contrat');
        $this->addSql('DROP TABLE new_contrat');
        $this->addSql('DROP TABLE admin_assureur');
        $this->addSql('DROP TABLE avis_client');
        $this->addSql('DROP TABLE avis_assureur');
        $this->addSql('DROP TABLE agent_assureur');
        $this->addSql('DROP TABLE offre');
    }
}
