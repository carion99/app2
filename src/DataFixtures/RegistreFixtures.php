<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\RegistreDeCommerce;

class RegistreFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        for ($i=0; $i < 100; $i++) { 
            $registre = new RegistreDeCommerce();

            $j = $i + 1;
            $nom = "assur".$j;    
            $imma = "regecom".rand(1000, 9999);
            

            $registre->setNom($nom)
                    ->setMatricule($imma);

            $manager->persist($registre);
        }

        $manager->flush();
    }
}
