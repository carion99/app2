<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\User;

use App\Entity\Root;
use App\Entity\AdminAssureur;
use App\Entity\AgentAssureur;
use App\Entity\CompteClient;

use App\Entity\CompteAssureur;

class UserFixtures extends Fixture
{
    private $encoder;

	public function __construct(UserPasswordEncoderInterface $encoder)
	{
		$this->encoder = $encoder;
	}
    
    /**
     * loading default user
     *
     * @param ObjectManager $manager
     * @param User $user1
     * @param User $user2
     * @param User $user3
     * @param Root $root
     * @param AdminAssureur $admin
     * @param AgentAssureur $agent
     * @param CompteClient $client
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        /**
         * Undocumented function
         *
         * @param string $motcle
         * @param integer $nb1
         * @param integer $nb2
         * @return void
         */
        function gencode(string $motcle, int $nb1, int $nb2)
        {
            $nb  = rand($nb1, $nb2);
            $code = $motcle.'-'.$nb;

            return $code;
        }

        $user1 = new User(); 
        $user2 = new User();
        $user3 = new User();
        $user4 = new User();

        $root = new Root();
        $admin = new AdminAssureur();
        $agent = new AgentAssureur();
        $client = new CompteClient();

        $assureur = new CompteAssureur();
        $codeassur = gencode("assureur", 1000, 9999);
        
        $coderoot   = gencode("root", 1, 9);
        $rootmail  = "root@gmail.com";

        $codeclient = gencode("client" , 1, 9);
        $clientmail= "client@gmail.com";

        $codeagent  = gencode("agent", 1, 9);
        $agentmail  = "agent@gmail.com";

        $codeadmin  = gencode("admin", 1, 9);
        $adminmail = "admin@gmail.com";

        $imma = gencode("assur", 1000, 5000);

        $nom = "medi"; $prenom = "yann michael";
        $motdepasse = "12345678"; $adresse = "Abidjan";
        $contact = 67623971;$nocni = "D 0156 0953 52";
        $travail = "Developpeur";

        $password1 = $this->encoder
        ->encodePassword($user1, $motdepasse);

        $password2 = $this->encoder
        ->encodePassword($user2, $motdepasse);

        $password3 = $this->encoder
        ->encodePassword($user3, $motdepasse);

        $password4 = $this->encoder
        ->encodePassword($user4, $motdepasse);

        $user1->setCodeUser($coderoot)
              ->setNom($nom)
              ->setPrenom($prenom)
              ->setUsername($rootmail)
              ->setPassword($password1)
              ->setRoles(["ROLE_ROOT"])
              ->setStatus("root");
        
        $user2->setCodeUser($codeadmin)
              ->setNom($nom)
              ->setPrenom($prenom)
              ->setUsername($adminmail)
              ->setPassword($password2)
              ->setRoles(["ROLE_ADMIN"])
              ->setStatus("admin");

        $user3->setCodeUser($codeclient)
              ->setNom($nom)
              ->setPrenom($prenom)
              ->setUsername($agentmail)
              ->setPassword($password3)
              ->setRoles(["ROLE_AGENT"])
              ->setStatus("agent");

        $user4->setCodeUser($codeagent)
              ->setNom($nom)
              ->setPrenom($prenom)
              ->setUsername($clientmail)
              ->setPassword($password4)
              ->setRoles(["ROLE_CLIENT"])
              ->setStatus("client");

        $root->setCodeRoot($coderoot)
             ->setNom($nom)
             ->setPrenom($prenom);
            
        $admin->setCodeAdminAssureur($codeadmin)
              ->setNom($nom)
              ->setPrenom($prenom)
              ->setEmail($adminmail)
              ->setPhone($contact)
              ->setAdresse($adresse)
              ->setNoCni($nocni)
              ->setCodeAssureur($codeassur);  

        $agent->setCodeAssureur($codeassur)
              ->setCodeAgentAssureur($codeagent)  
              ->setNom($nom)
              ->setPrenom($prenom)
              ->setRole("agent clientèle");

        $client->setCodeClient($codeclient)
               ->setNom($nom)
               ->setPrenom($prenom)
               ->setEmail($clientmail)
               ->setPhone1($contact)
               ->setPhone2(0)
               ->setProfession($travail);
      
        $assureur->setCodeAssureur($codeassur)
                 ->setNom("BG ASSUR")
                 ->setStatutJuridique("Société par Action Simplifié")
                 ->setEmail("bg@gmail.com")
                 ->setPhoneGsm(53198162)
                 ->setPhoneCel(0)
                 ->setPhoneFax(0)
                 ->setAdresse($adresse)
                 ->setNorCommerce("nor-".rand(100000, 999999))
                 ->setCodeRoot($coderoot)
                 ->setPermitRoot(1);


        $manager->persist($user1); 
        $manager->persist($user2);             
        $manager->persist($user3);    
        $manager->persist($user4);  

        $manager->persist($root);    
        $manager->persist($admin);    
        $manager->persist($agent);    
        $manager->persist($client); 

        $manager->persist($assureur);    

        $manager->flush();
    }
}
