<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use App\Entity\AdminAssureur;
use App\Entity\AgentAssureur;
use App\Entity\CompteAssureur;
use App\Entity\CompteClient;
use App\Entity\ContratAssureur;
use App\Entity\ModelContrat;
use App\Entity\User;
use App\Entity\Succes;
use App\Entity\AttenteValidation;

use App\Repository\UserRepository;
use App\Repository\RegistreDeCommerceRepository;
use App\Repository\CompteAssureurRepository;

class SecurityController extends AbstractController
{
    /**
     * @Route("/security1", name="security1")
     */
    public function index1()
    {
        return $this->render('security/index1.html.twig');
    }

    /**
     * @Route("/security2", name="security2")
     */
    public function index2()
    {
        return $this->render('security/index2.html.twig');
    }



    public function random($type, $car) {
        $string = "";
        $chaine = "abcdefghijklmnopqrstuvwxyz0123456789";
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
        $string .= $chaine[rand()%strlen($chaine)];
        }
        if ($type == 1) {
            $string = "atva_-".$string;
        }elseif ($type == 2){
            $string = "url*redirect_-".$string;
        }else{
            $string = "";
        }
        return $string;
    }

    public function genUrl(int $typeUrl){
        $alpha = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
                    "0","1","2","3","4","5","6","7","8","9",
                    "%","@","~","#"];   

        if ($typeUrl == 1) {
            $url = "atva_-".shuffle($alpha);
        }elseif($typeUrl == 2) {
            $url = "url*redirect_-".shuffle($alpha);
        }else{
            $url = "";
        }

        return $url;

    }

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
      $this->encoder = $encoder;
    }

    /**
     * @Route("/register_customer", name="register_client")
     */
    public function registerClient(Request $request, ObjectManager $manager, UserRepository $repouser)
    {
        function genCode($motcle){
            $nb  = rand(10000000, 99999999);
            $code = $motcle.'-'.$nb;

            return $code;
        }
        $error= "";

        if ($request->request->count() > 0) {

            
            $codeclient = genCode("client");
            $nom    = $request->request->get('nom');
            $prenom = $request->request->get('prenom');
            $phone1 = $request->request->get('phone1');
            $phone2 = $request->request->get('phone2');
            $travail= $request->request->get('travail');
            $email  = $request->request->get('email');
            $password1 = $request->request->get('password1');
            $password2 = $request->request->get('password2');

            $element["nom"]     = $nom;
            $element["prenom"]  = $prenom;
            $element["phone1"]  = $phone1;
            $element["phone2"]  = $phone2;
            $element["email"]   = $email;
            $element["travail"] = $travail;
            $error = "";

            $users = $repouser->findByUsername($email);
            $nbusers = count($users);

            if ($nbusers > 0) {
                $error = "Cet email a déja été utilisé";

                return $this->render('security/client/register.html.twig', [
                    'error' => $error,
                    'element' => $element
                ]);
            }else{
                if (empty($phone2)) {
                    $phone2 = 0;
                }

                if ($password2 == $password1) {

                    if (strlen($password1) >= 8) {

                        
                        $user = new User();
                        $client = new CompteClient();

                        $password = $this->encoder
                        ->encodePassword($user, $password1);
                        $roles = ["ROLE_CLIENT"];
                        $status = "client";

                        $user->setCodeUser($codeclient)
                            ->setNom($nom)
                            ->setPrenom($prenom)
                            ->setUsername($email)
                            ->setPassword($password)
                            ->setRoles($roles)
                            ->setStatus($status);   

                        $client->setCodeClient($codeclient)
                            ->setNom($nom)
                            ->setPrenom($prenom)
                            ->setProfession($travail)
                            ->setPhone1($phone1)
                            ->setPhone2($phone2)
                            ->setEmail($email);

                        $manager->persist($user);
                        $manager->persist($client);
                        //die();

                        $manager->flush();

                        return $this->redirectToRoute("login");
                    }else{
                        $error = "Votre mot de passe doit comprendre au moins 8 caractères.";

                        return $this->render('security/client/register.html.twig', [
                            'error' => $error,
                            'element' => $element
                        ]);
                    }
                }
                else {
                    $error = "mots de passe non identiques";

                    return $this->render('security/register.html.twig', [
                        'error' => $error,
                        'element' => $element
                    ]);
                }
            }

                



        }

        
        return $this->render('security/client/register0.html.twig', [
            'error' => $error
        ]);
    }

    
    /**
     * enrgistrement assureur (admin)
     * 
     * @Route("/register_assureur/1", name="register1_assureur")
     *
     * @param Request $request
     * @param ObjectManager $manager
     * @param \Swift_Mailer $mailer
     * @return void
     */
    public function registerAdminAssureur(Request $request, ObjectManager $manager, UserRepository $repouser, 
                                        \Swift_Mailer $mailer)
    {
        function genCode($motcle){

            $nb  = rand(100, 999);
            $code = $motcle.'-'.$nb;

            return $code;
        }

        /**
         * fonction de securisation des valeurs
         *
         * @param string $var
         * @return void
         */
        function filtre(string $var){
            $var = htmlentities(trim($var));

            return $var;
        }

        $error = "";
        $element = [
            "nom" => "",
            "prenom" => "",
            "email" => "",
            "adresse" => "",
            "phone" => "",
            "nocni" => "",
        ];

        if ($request->request->count() > 0) {

            $nom = $request->request->get('nom');
            $prenom = $request->request->get('prenom');
            $email = $request->request->get('email');
            $adresse = $request->request->get('adresse');
            $phone = $request->request->get('phone');
            $cni = $request->request->get('cni');
            $password1 = $request->request->get('password1');
            $password2 = $request->request->get('password2');

            $nom = filtre($nom);
            $prenom = htmlentities($prenom);
            $email = filtre($email);
            $adresse = htmlentities($adresse);
            $phone = intval($phone);
            $cni = filtre($cni);
            $password1 = filtre($password1);
            $password2 = filtre($password2);

            $element = [
                "nom" => $nom,
                "prenom" => $prenom,
                "email" => $email,
                "adresse" => $adresse,
                "phone" => $phone,
                "nocni" => $cni,
            ];

            $mailuser = $repouser->findByUsername($email);

            

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                if (filter_var($phone, FILTER_VALIDATE_INT)) {
                    if (strlen($phone) == 8) {
                        if ($password2 == $password1) {
                            if (strlen($password1) >= 8) {
                                if (count($mailuser) == 0) {

                                    $code1 = genCode("admin");
                                    $code2 = "assur"."-".rand(1000, 9999);
                                    
                                    $admin = new AdminAssureur();

                                    

                                    $admin->setCodeAdminAssureur($code1)
                                          ->setCodeAssureur($code2)  
                                          ->setNom(strtoupper($nom))
                                          ->setPrenom(strtoupper($prenom))
                                          ->setEmail($email)
                                          ->setPhone($phone)
                                          ->setAdresse(strtoupper($adresse))
                                          ->setNoCni($cni);

                                    $codeav = genCode("av");
                                    $urlav = $this->random(1,150);
                                    $urlred= $this->random(2,150);

                                    $attenteValidation = new AttenteValidation();

                                    $attenteValidation->setCodeAv($codeav)
                                                      ->setCodeuser($code1)
                                                      ->setUrlValidate($urlav)
                                                      ->setUrlRedirect($urlred)
                                                      ->setPassword($password1)
                                                      ->setUsername($email)
                                                      ->setStatus("admin")
                                                      ->setDateregister(new \DateTime()); 

                                   // $manager->persist($user);
                                    $manager->persist($attenteValidation);
                                    $manager->persist($admin);
                                    $manager->flush();
/*
                                    $pseudo = $nom;
                                    $mailroot = "mediyann99@gmail.com";
                                    $lien = "localhost";

                                    $message = (new \Swift_Message("coucou"))
                                                ->setFrom($mailroot)
                                                ->setTo($email)
                                                ->setBody(
                                                    $this->renderView(
                                                        'security/email/vue.html.twig',
                                                        [
                                                            '$pseudo' => $pseudo,
                                                            'lien' => $lien
                                                        ]
                                                    ),
                                                    'text/html'
                                                );
                                                
                                    $mailer->send($message);

 */                                        
                                    return $this->redirectToRoute("register2_assureur", ['code' => $code2]);

                                }else{
                                    $error = "cet email a déja été utilisé";
                                }
                            }else{
                                $error = "mot de passe doit contenir au moins 8 caractères";
                            }
                        }else{
                            $error = "mots de passe différents";
                        }
                    }else{
                        $error = "format de numéro incorrect";
                    }
                }else{
                    $error = "format de numéro incorrect";
                }
            }else{
                $error = "format d'email incorrect";
            }
        }
        
        
        return $this->render('security/assureur/register1.html.twig', [
            'error' => $error,
            'element' => $element
        ]);
    }

    /**
     * enregistrement assureur (societe)
     * 
     * @Route("/register_assureur/2/{code}", name="register2_assureur")
     *
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserRepository $repouser
     * @return void
     */
    public function registerSocieteAssureur($code, Request $request, ObjectManager $manager, 
    RegistreDeCommerceRepository $reponor, CompteAssureurRepository $societe)
    {
        /**
         * fonction de securisation des valeurs
         *
         * @param string $var
         * @return void
         */
        function filtre(string $var){
            $var = htmlentities(trim($var));

            return $var;
        }

        $error = "";
        $element = [
            "nom" => "",
            "statut" => "",
            "email" => "",
            "adresse" => "",
            "gsm" => "",
            "imma" => ""
        ];

        $repo = $manager->getRepository(AdminAssureur::class);
        $admin = $repo->findByCodeAssureur($code);
        $codeadmin = $admin[0]->getCodeAdminAssureur();

        if (count($admin) > 0) {
            
            if ($request->request->count() > 0) {
                
                $nom = $request->request->get('nom');
                $statut = $request->request->get('statut');
                $email = $request->request->get('email');
                $adresse = $request->request->get('adresse');
                $gsm = $request->request->get('gsm');
                $cel = $request->request->get('cel');
                $fax = $request->request->get('fax');
                $imma = $request->request->get('imma');

                $nom = filtre($nom);
                $statut = filtre($statut);
                $email = filtre($email);
                $adresse = htmlentities($adresse);
                $gsm = intval($gsm);
                $cel = intval($cel);
                $fax = intval($fax);
                $imma = filtre($imma);

                $element = [
                    "nom" => $nom,
                    "statut" => $statut,
                    "email" => $email,
                    "adresse" => $adresse,
                    "gsm" => $gsm,
                    "imma" => $imma
                ];

                $assur = $reponor->findByMatricule($imma);
                

                $tab = ["sa", "sam"];

                $comptass = $societe->findByCodeAssureur($code);

                if (count($comptass) <= 0) {
                    if (in_array($statut, $tab)) {
                        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                            if (filter_var($gsm, FILTER_VALIDATE_INT)) {
                                if (strlen($gsm) == 8) {
                                    if (count($assur) > 0) {
                                        $nassur = $assur[0]->getNom();
                                        if ($nassur == $nom) {
                                            
                                            $assureur = new CompteAssureur();

                                            if ($statut == "sa") {
                                                $statjuri = "societe anonyme";
                                            }else{
                                                $statjuri = "societe d'assurance mutuelle";
                                            }

                                            $assureur->setCodeAssureur($code)
                                                    ->setNom(strtoupper($nom))
                                                    ->setAdresse(strtoupper($adresse))
                                                    ->setStatutJuridique($statjuri)
                                                    ->setEmail($email)
                                                    ->setNorCommerce($imma)
                                                    ->setPhoneGsm($gsm);

                                            $succes = new Succes();
                                            $matricule = rand(100000000,999999999);
                                            
                                            $succes->setCodeAdmin($codeadmin)
                                                    ->setEtape1(1)
                                                    ->setEtape2(0)
                                                    ->setMatricule($matricule)
                                                    ->setDateRegister(new \DateTime());    

                                            $manager->persist($assureur);
                                            $manager->persist($succes);

                                            $manager->flush();

                                            return $this->redirectToRoute("succes", ['code' => $matricule]);

                                        }else{
                                            $error = "nom de societe invalide";
                                        }
                                    }else{
                                        $error = "Immatriculation inexistante";
                                    }
                                }else{
                                    $error = "numéro ivoirien svp, exemple: 54698752";
                                }
                            }else{
                                $error = "format de numéro incorrect";
                            }
                        }else{
                            $error = "format d'email incorrect";
                        }
                    }else{
                        $error = "valeur de statut juridique non repertoriée";
                    }
                }else{
                    $message = "attention_inscrit";
                    return $this->redirectToRoute('notif', ['message' => $message]);
                }  

            }
        }else{
            return $this->redirectToRoute('home');
        }


        return $this->render('security/assureur/register2.html.twig',[
            'error' => $error,
            'element' => $element,
            'code' => $code
        ]);
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $utils, ObjectManager $manager)
    {
        $error  = $utils->getLastAuthenticationError();
        $lastUsername = $utils->getLastUsername();

      return $this->render('security/login.html.twig', [
          'error' => $error,
          'last_username' => $lastUsername
      ]);
    }

    /**
     * redirection de compte en fonction du statut de l'utilisateur connecté
     * 
     * @Route("/compte", name="compte")
     *
     * @return void
     */
    public function redirectCompte()
    {
        $user = $this->getUser();
        if (isset($user)) {
            $statut = $user->getStatus();
            if ($statut == "root") {
                return $this->redirectToRoute('compte_root');
            }elseif ($statut == "admin") {
                return $this->redirectToRoute('compte_admin');
            }elseif ($statut == "agent") {
                return $this->redirectToRoute('compte_agent');
            }else{
                return $this->redirectToRoute('compte_client');
            }
        }else{
            return $this->redirectToRoute('home');
        }
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(){}
}
