<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\RelationOffreClient;
use App\Repository\RelationOffreClientRepository;
use App\Repository\AgentAssureurRepository;
use App\Repository\CompteClientRepository;
use App\Entity\AgentAssureur;
use App\Repository\AdminAssureurRepository;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Entity\Amessage;
use App\Repository\AmessageRepository;
use App\Entity\Cmessage;
use App\Repository\CmessageRepository;
use App\Repository\CompteAssureurRepository;
use App\Entity\Offre;
use App\Repository\OffreRepository;

use App\Entity\CGenerale;
use App\Repository\CGeneraleRepository;

use App\Entity\CParticuliere;
use App\Repository\CParticuliereRepository;

class CompteClientController extends AbstractController
{
    /**
     * @Route("/account/customer", name="compte_client")
     */
    public function index()
    {
        $active = "1";
        
        return $this->render('compte_client/index.html.twig', [
            'active' => $active
        ]);
    }

    /**
     * generateur de code pour les messages
     *
     * @param integer $typeMessage
     * @param float $value1
     * @param float $value2
     * @return void
     */
     public function genCode(int $typeMessage, float $value1, float $value2){
        switch ($typeMessage) {
            case 1:
                $code = "aaa.-".rand($value1, $value2);
                break;           
            case 2:
                $code = "bbb.-".rand($value1, $value2);
                break;
            case 3:
                $code = "ccc.-".rand($value1, $value2);
                break;
            default:
                $code = "";
                break;
        }
        return $code;
    }

    /**
     * @Route("/account/customer/assurances", name="listassur_client")
     */
    public function listeAssurance(RelationOffreClientRepository $relationOffreClientRepository,
    CompteClientRepository $compteClientRepository,
    AgentAssureurRepository $agentAssureurRepository,
    CompteAssureurRepository $compteAssureurRepository,
    OffreRepository $offreRepository)
    {
        $active = "2";

        $codeuser = $this->getUser()->getCodeUser();


        $offres = $relationOffreClientRepository->findByCodeClient($codeuser);
        $nbclients = count($offres);

        if ($nbclients > 0) {
            for ($i = 0; $i < $nbclients; $i++){
                //$codeclient = $clients[$i]->getCodeClient();
                $codeoffre = $offres[$i]->getCodeOffre();
                $codeassur = $offres[$i]->getCodeAssureur();
                $datereg = $offres[$i]->getDateRegister();
                $intitule = $offreRepository->findByCodeOffre($codeoffre)[0]
                ->getIntitule();

                $nomassur = $compteAssureurRepository->findByCodeAssureur($codeassur)[0]
                ->getNom();

                $pack = [
                    'codeoffre' => $codeoffre,
                    'dateregister' => $datereg,
                    'titre' => $intitule,
                    'codeassur' => $codeassur,
                    'nom' => $nomassur
                ];
                $packs[$i] = $pack;

            }
            
            return $this->render('compte_client/assurance.html.twig', [
                'active' => $active,
                'packs' => $packs
            ]);
        }else{
            return $this->render('compte_client/assurance2.html.twig', [
                'active' => $active
            ]);
        }

    }

    /**
     * @Route("/account/customer/comparator", name="comparateur_client")
     */
    public function comparateur( CompteAssureurRepository $compteAssureurRepository , 
                                    OffreRepository $offreRepository)
    {
        $active = "3";

        $contrats = $offreRepository->findAll();
        dump($contrats);


        
        
        if(count($contrats) > 0){

            for ($i = 0; $i < count($contrats); $i++){
                $codeassur = $contrats[$i]->getCodeAssureur();
                $assureur = $compteAssureurRepository->findByCodeAssureur($codeassur)[0];
                $nom = $assureur->getNom();

                $gcontrats[$i] = [
                    'nom' => $nom,
                    'contrat' => $contrats[$i]
                ]; 
            }

            return $this->render('compte_client/comparateur.html.twig', [
                'active' => $active,
                'gcontrats' => $gcontrats
            ]);
        }else{
            return $this->render('compte_client/comparateur2.html.twig', [
                'active' => $active
            ]);
        }

    }

    /**
     * @Route("/account/customer/notifications", name="notifications_client")
     */
    public function notifications()
    {
        $active = "4";
        
        return $this->render('compte_client/notification.html.twig', [
            'active' => $active
        ]);
    }

    /**
     * @Route("/account/customer/infos", name="infos_client")
     */
    public function infos()
    {
        $active = "5";
        
        return $this->render('compte_client/infoprofil.html.twig', [
            'active' => $active
        ]);
    }


    /**
     * permet de contacter l'agent en charge du client
     * 
     * @Route("/account/customer/listagent/{codeassur}", name="suivre_agent_assur")
     *
     * @return void
     */
    public function suivreAgentAssur($codeassur, AdminAssureurRepository $adminAssureurRepository,
                                        AgentAssureurRepository $agentAssureurRepository)
    {
        $active = "2";

        $admins = $adminAssureurRepository->findByCodeAssureur($codeassur);
        $nbadmins = count($admins);
        

        if ($nbadmins > 0) {
            $codeadmin = $admins[0]->getCodeAdminAssureur();

            $agents = $agentAssureurRepository->findByCodeAdminAssureur($codeadmin);
            return $this->render('compte_client/listagent.html.twig', [
                'active' => $active,
                'agents' => $agents
            ]);
        }else{
            return $this->redirectToRoute("listassur_client");
        }
        
        
    }

    /**
     * permet de contacter l'agent en charge du client
     * 
     * @Route("/account/customer/agent/{codeagent}", name="suivre_agent")
     *
     * @return void
     */
    public function suivreAgent($codeagent, UserRepository $userRepository,
     AgentAssureurRepository $agentAssureurRepository,
     CmessageRepository $cmessageRepository,
     ObjectManager $manager,
     Request $request)
    {
        $active = "2";

        $agents = $agentAssureurRepository->findByCodeAgentAssureur($codeagent);
        $nbagents = count($agents);

        if ($nbagents > 0) {
            $agent= $agents[0];
            $destinataire =  $userRepository->findByCodeUser($codeagent)[0]
            ->getUsername();

            $destinateur = $this->getUser()->getUsername();

            if ($request->request->count() > 0) {
                $message = $request->request->get('message');
                
                $cmessage = new Cmessage();
                $codemsg = $this->genCode(3, 0, 2000);
    
                $cmessage->setCodeMsg($codemsg)
                         ->setDestinateur($destinateur)
                         ->setDestinataire($destinataire)
                         ->setContenu($message);
    
                $manager->persist($cmessage);
                $manager->flush();
    
                $cmessages = $cmessageRepository->findAll();
    
                dump($cmessages);
    
                return $this->render('compte_client/agent.html.twig', [
                    'messages' => $cmessages,
                    'destinateur' => $destinateur,
                    'destinataire' => $destinataire,
                    'codeagent' => $codeagent,
                    'active' => $active
                ]);
    
            }
            $cmessages = $cmessageRepository->findAll();
            return $this->render('compte_client/agent.html.twig', [
                'active' => $active,
                'destinateur' => $destinateur,
                'destinataire' => $destinataire,
                'codeagent' => $codeagent,
                'messages' => $cmessages,
            ]);
        }else{
            return $this->redirectToRoute("listassur_client");
        }

        
        return $this->render('compte_client/agent.html.twig', [
            'active' => $active
        ]);
    }
}
