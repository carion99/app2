<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use App\Repository\SuccesRepository;
use App\Entity\AttenteValidation;
use App\Repository\AttenteValidationRepository;
use App\Repository\AdminAssureurRepository;
use App\Repository\CompteAssureurRepository;
use App\Repository\RootRepository;
use PhpParser\Node\Stmt\Echo_;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig');
    }

    /**
     * @Route("/test", name="test")
     */
    public function test()
    {
        return $this->render('home/pages/ok.html.twig');
    }

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
      $this->encoder = $encoder;
    }

    /**
     * @Route("/success/{code}", name="succes")
     */
    public function succes($code, SuccesRepository $success, AttenteValidationRepository $attenteValidationRepository)
    {
        $valid = $success->findByMatricule($code);
        
        $codeuser = $valid[0]->getCodeAdmin();
        $attente = $attenteValidationRepository->findByCodeuser($codeuser);

        $codeav = $attente[0]->getCodeAv();
        $urlred= $attente[0]->getUrlRedirect();

        dump($attente);
        $nb = count($valid);

        $texte = "Votre Société a été enregistrée avec succès"; 

        if ($nb > 0) {
            return $this->render('home/pages/succes.html.twig',[
                'texte' => $texte,
                'url' => $urlred
            ]);
        }else{
            return $this->redirectToRoute('home');
        }
    }

    
    /**
     * notifications
     * 
     * @Route("/notification/{message}", name="notif")
     *
     * @param string $message
     * @return void
     */
    public function notification(string $message = "")
    {
        if ($message == "attention_inscrit") {
            $texte = "Attention, Cette Société d'assurance a déja été enregistrée ou alors vous essayer de frauder.";
        }else{
            $texte = "nothing";
        }
        
        return $this->render('home/pages/notif.html.twig',[
            'texte' => $texte
        ]);
    }

    /**
     * validation function
     * 
     * @Route("valid/{url}", name="validation")
     *
     * @return void
     */
    public function validation($url, AttenteValidationRepository $attenteValidationRepository){
        $attente = $attenteValidationRepository->findByUrlRedirect($url); 


        if (count($attente) > 0) {
            $urlav = $attente[0]->getUrlValidate();
            return $this->render('home/pages/valid.html.twig', [
                'url' => $urlav
            ]);
        }else{
            return $this->redirectToRoute("home");
        }
    }
    
    /**
     * Undocumented function
     * 
     * @Route("end/{url}", name="insertok")
     *
     * @param [type] $url
     * @return void
     */
    public function insertok($url, ObjectManager $manager,
                                RootRepository $rootRepository,
                                AdminAssureurRepository $adminAssureurRepository,
                                CompteAssureurRepository $compteAssureurRepository,
                                AttenteValidationRepository $attenteValidationRepository){

        $attente = $attenteValidationRepository->findByUrlValidate($url); 
        
        

        if (count($attente) > 0) {
            $username = $attente[0]->getUsername();
            $password = $attente[0]->getPassword();
            $status = $attente[0]->getStatus();
            $codeadmin = $attente[0]->getCodeuser();

            $admin = $adminAssureurRepository->findByCodeAdminAssureur($codeadmin);


            $nom = $admin[0]->getNom();
            $prenom = $admin[0]->getPrenom();   
            $codeassur = $admin[0]->getCodeAssureur();

            $user = new User();
            $password = $this->encoder
                            ->encodePassword($user, $password);
            $roles = ["ROLE_ADMIN"]; 

            $root = $rootRepository->findAll()[0];
            $coderoot = $root->getCodeRoot();

            $assureur = $compteAssureurRepository->findByCodeAssureur($codeassur)[0];
            

            $assureur->setCodeRoot($coderoot)
                     ->setPermitRoot(1);

            $user->setCodeUser($codeadmin)
                ->setUsername($username)
                ->setPassword($password)
                ->setNom($nom)
                ->setPrenom($prenom)
                ->setRoles($roles)  
                ->setStatus($status); 
            
            $id = $attente[0]->getId();  
            $att= $attenteValidationRepository->find($id);  
            
            $manager->persist($user);
            $manager->remove($att);
            $manager->flush();

            
            $manager->flush();


            return $this->render('home/pages/ok.html.twig');
        }else{
            return $this->redirectToRoute("home");
        }
        
    }

}
