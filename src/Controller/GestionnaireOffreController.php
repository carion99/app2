<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Offre;
use App\Repository\OffreRepository;

use App\Entity\CGenerale;
use App\Repository\CGeneraleRepository;

use App\Entity\CParticuliere;
use App\Repository\CParticuliereRepository;
use App\Repository\AdminAssureurRepository;
use App\Repository\CompteAssureurRepository;
use App\Entity\RelationOffreClient;
use App\Repository\RelationOffreClientRepository;

class GestionnaireOffreController extends AbstractController
{
    /**
     * @Route("/gestionnaire/offre", name="gestionnaire_offre")
     */
    public function index()
    {
        return $this->render('gestionnaire_offre/index.html.twig', [
            'controller_name' => 'GestionnaireOffreController',
        ]);
    }

    /**
     * @Route("/account/admin/contract/new", name="newoffre")
     */
    public function newContrat(Request $request,
                                AdminAssureurRepository $adminAssureurRepository, 
                                CompteAssureurRepository $compteAssureurRepository,
                                ObjectManager $objectManager)
    {
        $active = 5;

        if ($request->request->count() > 0) {  
            $offre = new Offre();

            $codeadmin = $this->getUser()->getCodeUser();          
            $intitule = $request->request->get('intitule');
            $domaine = $request->request->get('domaine');

            $codeassur = $adminAssureurRepository->findByCodeAdminAssureur($codeadmin)[0]
            ->getCodeAssureur();

            $offre->setCodeAssureur($codeassur)
            ->setIntitule($intitule)
            ->setDomaine($domaine);

            $objectManager->persist($offre);
            $objectManager->flush();
 
           return $this->redirectToRoute("infos_contrat");
        }
        
        return $this->render('compte_admin_assureur/pages/newcontrat.html.twig', [
            'active' => $active
        ]);
    }

    /**
     * @Route("/gestionnaire/offre", name="gestionnaire_offre")
     */
    public function listeContrat(OffreRepository $offreRepository)
    {
        
        
        $active = 5;

        $contrats = $offreRepository->findAll();
        
        return $this->render('compte_admin_assureur/contrat.html.twig', [
            'active' => $active,
            'offres' => $contrats
        ]);
    }

    /**
     * @Route("/gestionnaire/offre", name="gestionnaire_offre")
     */
    public function modifierContrat(Request $request, 
                                    OffreRepository $offreRepository,
                                    ObjectManager $objectManager)
    {
        return $this->redirectToRoute("infos_contrat");
    }

    /**
     * @Route("/gestionnaire/offre", name="gestionnaire_offre")
     */
    public function supprimerContrat(OffreRepository $offreRepository,
                                        ObjectManager $objectManager)
    {
        return $this->redirectToRoute("infos_contrat");;
    }

    /**
     * @Route("/account/customer/souscripoffer/{codeoffre}", name="souscrip_offre")
     */
    public function souscripOffre($codeoffre, 
                                    ObjectManager $objectManager,
                                    OffreRepository $offreRepository,
                                    AdminAssureurRepository $adminAssureurRepository,
                                    RelationOffreClientRepository $relationOffreClientRepository)
    {
        $reloffres = $relationOffreClientRepository->findByCodeOffre($codeoffre);
        $nbrel = count($reloffres);

        if ($nbrel == 0) {
            $codeuser = $this->getUser()->getCodeUser();

            $codeassur = $offreRepository->findByCodeOffre($codeoffre)[0]
            ->getCodeAssureur();

            $codeadmin = $adminAssureurRepository->findByCodeAssureur($codeassur)[0]
            ->getCodeAdminAssureur();

            $reloc = new RelationOffreClient();
            $reloc->setCodeClient($codeuser)
            ->setCodeOffre($codeoffre)
            ->setCodeAdminAssureur($codeadmin)
            ->setCodeAssureur($codeassur);

            $objectManager->persist($reloc);
            $objectManager->flush();    


            return $this->redirectToRoute("listassur_client");

        }else{
            return $this->redirectToRoute("listassur_client");
        }
    }

}
