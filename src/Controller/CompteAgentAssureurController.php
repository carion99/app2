<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\AgentAssureurRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

use App\Repository\UserRepository;
use App\Entity\Bmessage;
use App\Repository\BmessageRepository;
use App\Entity\Cmessage;
use App\Repository\CmessageRepository;
use App\Repository\CompteAssureurRepository;
use App\Repository\CompteClientRepository;
use App\Entity\RelationOffreClient;
use App\Repository\RelationOffreClientRepository;
use App\Entity\AgentAssureur;
use App\Repository\OffreRepository;

class CompteAgentAssureurController extends AbstractController
{
    /**
     * @Route("/account/agent", name="compte_agent")
     */
    public function index()
    {
        $active = 1;
        
        return $this->render('compte_agent_assureur/index.html.twig', [
            'active' => $active
        ]);
    }

    /**
     * generateur de code pour les messages
     *
     * @param integer $typeMessage
     * @param float $value1
     * @param float $value2
     * @return void
     */
     public function genCode(int $typeMessage, float $value1, float $value2){
        switch ($typeMessage) {
            case 1:
                $code = "aaa.-".rand($value1, $value2);
                break;           
            case 2:
                $code = "bbb.-".rand($value1, $value2);
                break;
            case 3:
                $code = "ccc.-".rand($value1, $value2);
                break;
            default:
                $code = "";
                break;
        }
        return $code;
    }

    /**
     * @Route("/account/agent/customer", name="liste_client")
     */
    public function listeClient(RelationOffreClientRepository $relationOffreClientRepository,
                                 CompteClientRepository $compteClientRepository,
                                 AgentAssureurRepository $agentAssureurRepository,
                                 CompteAssureurRepository $compteAssureurRepository,
                                 OffreRepository $offreRepository)
    {
        $active = 2;
        $codeagent = $this->getUser()->getCodeUser();
        $codeadmin = $agentAssureurRepository->findByCodeAgentAssureur($codeagent)[0]
        ->getCodeAdminAssureur();

        $clients = $relationOffreClientRepository->findByCodeAdminAssureur($codeadmin);
        $nbclients = count($clients);

        if ($nbclients > 0) {
            for ($i = 0; $i < $nbclients; $i++){
                $codeclient = $clients[$i]->getCodeClient();
                $codeoffre = $clients[$i]->getCodeOffre();
                $codeassur = $clients[$i]->getCodeAssureur();
                $datereg = $clients[$i]->getDateRegister();
                $intitule = $offreRepository->findByCodeOffre($codeoffre)[0]
                ->getIntitule();
                
                $pack = [
                    'codeclient' => $codeclient,
                    'codeoffre' => $codeoffre,
                    'dateregister' => $datereg,
                    'titre' => $intitule,
                    'codeassur' => $codeassur
                ];
                $packs[$i] = $pack;

            }
            
            return $this->render('compte_agent_assureur/liste.html.twig', [
                'active' => $active,
                'packs' => $packs
            ]);
        }else{
            return $this->render('compte_agent_assureur/liste2.html.twig', [
                'active' => $active
            ]);
        }
        
    }

    /**
     * @Route("/account/agent/admin", name="suivre_admin")
     */
    public function suivreAdmin(Request $request, ObjectManager $manager,
                                UserRepository $userRepository, 
                                AgentAssureurRepository $agentAssureurRepository,
                                BmessageRepository $bmessageRepository)
    {
        $active = 3;

        $codeagent = $this->getUser()->getCodeUser();
        $bmessages = $bmessageRepository->findAll();
        $destinateur = $this->getUser()->getUsername();
        $codeadmin = $agentAssureurRepository->findByCodeAgentAssureur($codeagent)[0]
                                               ->getCodeAdminAssureur();
        
        $destinataire = $userRepository->findByCodeUser($codeadmin)[0]->getUsername();

        if ($request->request->count() > 0) {
            $message = $request->request->get('message');
            
            $bmessage = new Bmessage();
            $codemsg = $this->genCode(2, 0, 2000);

            $bmessage->setCodeMsg($codemsg)
                     ->setDestinateur($destinateur)
                     ->setDestinataire($destinataire)
                     ->setContenu($message);

            $manager->persist($bmessage);
            $manager->flush();

            $bmessages = $bmessageRepository->findAll();

            dump($bmessages);

            return $this->render('compte_agent_assureur/admin.html.twig', [
                'messages' => $bmessages,
                'active' => $active
            ]);

        }




        if ($request->isXmlHttpRequest()) {
            $message = $request->get('message');
            $destinateur = $request->get('destinateur');
            $destinataire = $request->get('destinataire');
            $data = [
                'emit' => $destinateur,
                'reci' => $destinataire,
                'texto'=> $message
            ];
            $username = $this->getUser()->getUsername();
            $codemsg = genCode(3,0,1000000);
            $jsondata = array($data);
            dump($request);

            return $this->json($jsondata);

        }

             
            

            dump($bmessages);

            return $this->render('compte_agent_assureur/admin.html.twig', [
                'active' => $active,
                'destinateur' => $destinateur,
                'destinataire' => $destinataire,
                'messages' => $bmessages
        ]);
        

        
    }

    /**
     * @Route("/account/agent/notifications", name="notifications_agent")
     */
    public function notifications()
    {
        $active = 4;
        
        return $this->render('compte_agent_assureur/notification.html.twig', [
            'active' => $active
        ]);
    }

    /**
     * @Route("/account/agent/infos", name="infos_agent")
     */
    public function infos()
    {
        $active = 5;
        
        return $this->render('compte_agent_assureur/infoprofil.html.twig', [
            'active' => $active
        ]);
    }

    /**
     * @Route("/account/agent/customer/{codeclient}", name="suivre_client")
     */
    public function suivreClient($codeclient, Request $request, ObjectManager $manager,
    UserRepository $userRepository, 
    AgentAssureurRepository $agentAssureurRepository,
    CmessageRepository $cmessageRepository)
    {
        $active = 2;
        
        $codeagent = $this->getUser()->getCodeUser();
        $cmessages = $cmessageRepository->findAll();

        $clients = $userRepository->findByCodeUser($codeclient);
        $nbclients = count($clients);

        if ($nbclients > 0) {
            $destinateur = $this->getUser()->getUsername();

        
            $destinataire = $clients[0]->getUsername();

            if ($request->request->count() > 0) {
                $message = $request->request->get('message');
                
                $cmessage = new Cmessage();
                $codemsg = $this->genCode(2, 0, 2000);

                $cmessage->setCodeMsg($codemsg)
                        ->setDestinateur($destinateur)
                        ->setDestinataire($destinataire)
                        ->setContenu($message);

                $manager->persist($cmessage);
                $manager->flush();

                $cmessages = $cmessageRepository->findAll();

                dump($cmessages);

                return $this->render('compte_agent_assureur/client.html.twig', [
                    'messages' => $cmessages,
                    'active' => $active,
                    'destinateur' => $destinateur,
                    'destinataire' => $destinataire,
                    'codeclient' => $codeclient
                ]);

            }

            return $this->render('compte_agent_assureur/client.html.twig', [
                'messages' => $cmessages,
                'active' => $active,
                'destinateur' => $destinateur,
                'destinataire' => $destinataire,
                'codeclient' => $codeclient
            ]);
        }else{
            return $this->redirectToRoute("liste_client");
        }


        
    }
}
