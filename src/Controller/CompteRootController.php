<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Repository\AdminAssureurRepository;
use App\Repository\CompteAssureurRepository;
use App\Repository\CompteClientRepository;
use App\Repository\AgentAssureurRepository;

use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\UserRepository;
use App\Entity\User;
use App\Repository\AmessageRepository;
use App\Entity\Amessage;

class CompteRootController extends AbstractController
{
    /**
     * @Route("/account/root", name="compte_root")
     */
    public function index()
    {
        $active = 1;
        
        return $this->render('compte_root/index.html.twig', [
            'active' => $active
        ]);
    }

    /**
     * generateur de code pour les messages
     *
     * @param integer $typeMessage
     * @param float $value1
     * @param float $value2
     * @return void
     */
    public function genCode(int $typeMessage, float $value1, float $value2){
        switch ($typeMessage) {
            case 1:
                $code = "aaa-".rand($value1, $value2);
                break;           
            case 2:
                $code = "bbb-".rand($value1, $value2);
                break;
            case 3:
                $code = "ccc-".rand($value1, $value2);
                break;
            default:
                $code = "";
                break;
        }
        return $code;
    }

    /**
     * @Route("/account/root/gestion_assureur", name="gestion_assureur")
     */
    public function gestionAssureur(AdminAssureurRepository $adminAssureurRepository,
                                    CompteAssureurRepository $compteAssureurRepository) 
    {
        $active = 2;

        $admins = $adminAssureurRepository->findAll();
        

        
        return $this->render('compte_root/gestion_assureur.html.twig', [
            'active' => $active,
            'admins' => $admins
        ]);
    }

    /**
     * @Route("/account/root/gestion_assureur/{code}", name="suivre_assureur")
     */
    public function suivreAssureur($code, ObjectManager $manager,
                                    Request $request,
                                    UserRepository $userRepository,
                                    AmessageRepository $amessageRepository,
                                    AdminAssureurRepository $adminAssureurRepository,
                                    CompteAssureurRepository $compteAssureurRepository) 
    {
        $active = 2;


        $admin = $userRepository->findByCodeUser($code);
        $amessages = $amessageRepository->findAll();
        $destinateur = $this->getUser()->getUsername();

        if (count($admin) > 0) {
            $destinataire = $admin[0]->getUsername();
            dump($amessages);
            //die();
        }else{
            return $this->redirectToRoute("gestion_assureur");
        }
        

        if ($request->request->count() > 0) {
            $amessages = $amessageRepository->findAll();
            $message = $request->request->get('message');
            $amessage =  new Amessage();
            $codemsg = $this->genCode(1, 0, 2000);

            $amessage->setCodeMsg($codemsg)
            ->setDestinateur($destinateur)
            ->setDestinataire($destinataire)
            ->setContenu($message);

            dump($amessage);


            $manager->persist($amessage);
            $manager->flush();

            dump($amessages);

            return $this->render('compte_root/pages/chat.html.twig', [
                'active' => $active,
                'code' => $code,
                'destinateur' => $destinateur,
                'destinataire' => $destinataire,
                'messages' => $amessages
            ]);
        }

        

        return $this->render('compte_root/pages/chat.html.twig', [
            'active' => $active,
            'code' => $code,
            'destinateur' => $destinateur,
            'destinataire' => $destinataire,
            'messages' => $amessages
        ]);

        
    
        
        
    }

    /**
     * @Route("/account/root/gestion_agent", name="gestion_agent_root")
     */
    public function gestionAgent(UserRepository $userRepository,
                                    AgentAssureurRepository $agentAssureurRepository,
                                    ObjectManager $objectManager)
    {
        $active = 3;

        $agents = $agentAssureurRepository->findAll();
        
        return $this->render('compte_root/gestion_agent.html.twig', [
            'active' => $active,
            'agents' => $agents
        ]);
    }

    /**
     * @Route("/account/root/gestion_client", name="gestion_client_root")
     */
    public function gestionClient(UserRepository $userRepository,
                                    CompteClientRepository $compteClientRepository,
                                    ObjectManager $objectManager)
    {
        $active = 4;


        $clients = $compteClientRepository->findAll();
        
        return $this->render('compte_root/gestion_client.html.twig', [
            'active' => $active,
            'clients' => $clients
        ]);
    }

    /**
     * @Route("/account/root/gestion_prospect", name="gestion_prospect")
     */
    public function gestionProspect()
    {
        $active = 5;
        
        return $this->render('compte_root/gestion_prospect.html.twig', [
            'active' => $active
        ]);
    }

    /**
     * @Route("/account/root/notifications", name="notifications_root")
     */
    public function notifications(AdminAssureurRepository $repoAdmin, CompteAssureurRepository $repoAssureur)
    {
        $active = 6;

        $listAssureur = $repoAssureur->findAll();

        $a = 0;
        foreach ($listAssureur as $assur) {
            $coderoot = $assur->getCodeRoot();
            $permitroot = $assur->getPermitRoot();
            if ($coderoot == "nothing" && $permitroot == 0) {
                $newlistAssureur[$a] = $assur;

                $codeAssureur = $assur->getCodeAssureur();

                $assureur = $repoAssureur->findByCodeAssureur($codeAssureur);
                $assureur = $assureur[0];

            }
        }
        
        return $this->render('compte_root/notification.html.twig', [
            'active' => $active
        ]);
    }

    /**
     * @Route("/account/root/infos", name="infos_root")
     */
    public function infoRoot()
    {
        $active = 7;
        
        return $this->render('compte_root/infoprofil.html.twig', [
            'active' => $active
        ]);
    }

    /**
     * @Route("/account/root/search", name="search_root")
     */
    public function rechercher(Request $request, UserRepository $repoUser, 
    CompteAssureurRepository $repoAssureur, AdminAssureurRepository $repoAdmin)
    {
        $active = 8;

        if ($request->isXmlHttpRequest()) {

            $req = $request->get('search');

            $listUser = $repoUser->findByCodeUser($req);

            if (count($listUser) > 0){
                $user = $listUser[0];
                $status = $user->getStatus();

                if ($status == "root") {
                    $codeuser = $user->getCodeUser();
                    $nom = $user->getNom();
                    $prenom = $user->getPrenom();
                    $ident = $nom." ".$prenom;
                    $statut = "Super Administrateur";
                    $date = $user->getDateRegister();
                    $tlien = "modif non possible";
                    $lien = "path('search_root')";

                    $info = [
                        'code'   => $codeuser,
                        'ident'  => $ident,
                        'statut' => $statut,
                        'date'   => $date,
                        'tlien'  => $tlien,
                        'lien'   => $lien
                    ];
                }elseif ($status == "client") {
                    $codeuser = $user->getCodeUser();
                    $nom = $user->getNom();
                    $prenom = $user->getPrenom();
                    $ident = $nom." ".$prenom;
                    $statut = "Client";
                    $date = $user->getDateRegister();
                    $tlien = "modif non possible";
                    $lien = "path('search_root')";

                    $info = [
                        'code'   => $codeuser,
                        'ident'  => $ident,
                        'statut' => $statut,
                        'date'   => $date,
                        'tlien'  => $tlien,
                        'lien'   => $lien
                    ];
                }elseif ($status == "agent") {
                    $codeuser = $user->getCodeUser();
                    $nom = $user->getNom();
                    $prenom = $user->getPrenom();
                    $ident = $nom." ".$prenom;
                    $statut = "Agent Assureur";
                    $date = $user->getDateRegister();
                    $tlien = "modif non possible";
                    $lien = "path('search_root')";

                    $info = [
                        'code'   => $codeuser,
                        'ident'  => $ident,
                        'statut' => $statut,
                        'date'   => $date,
                        'tlien'  => $tlien,
                        'lien'   => $lien
                    ];
                }else {
                    $codeuser = $user->getCodeUser();
                    $nom = $user->getNom();
                    $prenom = $user->getPrenom();
                    $ident = $nom." ".$prenom;
                    $statut = "Admin Assureur";
                    $date = $user->getDateRegister();

                    $listAdmin = $repoAdmin->findByCodeAdminAssureur($codeuser);
                    $admin = $listAdmin[0];

                    $codeassur = $admin->getCodeAssureur();

                    $listAssureur = $repoAssureur->findByCodeAssureur($codeassur);
                    $assureur = $listAssureur[0];

                    $permitroot = $assureur->getPermitRoot();

                    switch ($permitroot) {
                        case 0:
                            $tlien = "Valider l'inscription ?";
                            $lien = "account/root/valid/".$codeuser;
                            break;
                        case 1:
                            $tlien = "modif non possible : permitroot = ".$permitroot;
                            $lien = "/account/root/search";
                            break;                     
                        case 2:
                            $tlien = "modif non possible : permitroot = ".$permitroot;
                            $lien = "/account/root/search";
                            break;
                        
                        default:
                            # code...
                            break;
                    }


                    $info = [
                        'code'   => $codeuser,
                        'ident'  => $ident,
                        'statut' => $statut,
                        'date'   => $date,
                        'tlien'  => $tlien,
                        'lien'   => $lien
                    ];
                }

            }else{
                $user = [];
                $info = [];
            }     

            $jsondata = array($info);

            return $this->json($jsondata);

            //return new JsonResponse($jsondata);  
        }

        //dump($request);
           // die();
        
        return $this->render('compte_root/search.html.twig', [
            'active' => $active
        ]);
    }
    
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
      $this->encoder = $encoder;
    }

    /**
     * Undocumented function
     * 
     * @Route("/account/root/valid/{code}", name="valid_root")
     *
     * @param [type] $code
     * @return void
     */
    public function valid($code, User $user, 
                            ObjectManager $manager, 
                            AdminAssureurRepository $repoAdmin, 
                            CompteAssureurRepository $repoAssureur)
    {
        $listAdmin = $repoAdmin->findByCodeAdminAssureur($code);
        $admin = $listAdmin[0];

        if (isset($admin)) {
            $codeassur = $admin->getCodeAssureur();
            $listAssureur = $repoAssureur->findByCodeAssureur($codeassur);
            $assureur = $listAssureur[0];

            $coderoot = $this->getUser()->getCodeUser();

            $assureur->setPermitRoot(1)
                     ->setCodeRoot($coderoot);
            $manager->persist($assureur);

            $nom = $admin->getNom();
            $prenom = $admin->getPrenom();
            $email = $admin->getEmail();
            $motdepasse = $admin->getMotedepasse();
            $password = $this->encoder
                    ->encodePassword($user, $motdepasse);


            $user->setCodeUser($code)
                 ->setNom($nom)
                 ->setPrenom($prenom)
                 ->setUsername($email)
                 ->setPassword($password)
                 ->setRoles(["ROLE_ADMIN"])
                 ->setStatus("admin");
            $manager->persist($user);

            $manager->flush();  
            
            return $this->redirectToRoute("gestion_assureur");
            
        }else{
            return $this->redirectToRoute("compte_root");
        }
    }

    /**
     * Undocumented function
     * 
     * @Route("/account/root/gestion_admin/rm/{codeadmin}", name="rm_admin")
     *
     * @param [type] $codeadmin
     * @param ObjectManager $objectManager
     * @param AdminAssureurRepository $agentAssureurRepository
     * @return void
     */
    public function removeAdmin($codeadmin,
    UserRepository $userRepository, 
    ObjectManager $objectManager,
    AdminAssureurRepository $adminAssureurRepository,
    AgentAssureurRepository $agentAssureurRepository,
    CompteAssureurRepository $compteAssureurRepository){

        $user = $userRepository->find($codeadmin);
        $assureur = $compteAssureurRepository->find($codeadmin);
        $admin = $adminAssureurRepository->find($codeadmin);
        $agents = $agentAssureurRepository->findByCodeAdminAssureur($codeadmin);

        
        $objectManager->remove($user);
        $objectManager->remove($admin);
        $objectManager->remove($assureur);
        foreach ($agents as $agent) {
            $objectManager($agent);
        }
        $objectManager->flush();

        return $this->redirectToRoute("gestion_admin");

    }

    /**
     * Undocumented function
     * 
     * @Route("/account/root/gestion_agent/rm/{codeagent}", name="rm_agent")
     *
     * @param [type] $codeagent
     * @param ObjectManager $objectManager
     * @param AgentAssureurRepository $adminAssureurRepository
     * @return void
     */
    public function removeAgent($codeagent,
    UserRepository $userRepository, 
    ObjectManager $objectManager,
    AgentAssureurRepository $agentAssureurRepository){

        $user = $userRepository>find($codeagent);
        $agent = $agentAssureurRepository->find($codeagent);
        $objectManager->remove($user);
        $objectManager->remove($agent);
        $objectManager->flush();

        return $this->redirectToRoute("gestion_agent");
        
    }

    /**
     * Undocumented function
     * 
     * @Route("/account/root/gestion_client/rm/{codeclient}", name="rm_client")
     *
     * @param [type] $codeclient
     * @param ObjectManager $objectManager
     * @param CompteClientRepository $compteClientRepository
     * @return void
     */
    public function removeClient($codeclient, 
    UserRepository $userRepository,
                                    ObjectManager $objectManager,
                                    CompteClientRepository $compteClientRepository){

        
        $user  = $userRepository->find($codeclient);                               
        $client = $compteClientRepository->find($codeclient);
        dump($codeclient,$user,$client);die();
        $objectManager->remove($user);
        $objectManager->remove($client);
        $objectManager->flush();

        return $this->redirectToRoute("gestion_client");

    }
    
}
