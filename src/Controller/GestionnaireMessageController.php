<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;

use App\Entity\Amessage;
use App\Repository\AmessageRepository;

use App\Entity\Bmessage;
use App\Repository\BmessageRepository;

use App\Entity\Cmessage;
use App\Repository\CmessageRepository;
use Doctrine\Common\Persistence\ObjectManager;

class GestionnaireMessageController extends AbstractController
{
    /**
     * @Route("/gestionnaire/message", name="gestionnaire_message")
     */
    public function index()
    {
        return $this->render('gestionnaire_message/index.html.twig', [
            'controller_name' => 'GestionnaireMessageController',
        ]);
    }
    
    /**
     * generateur de code pour les messages
     *
     * @param integer $typeMessage
     * @param float $value1
     * @param float $value2
     * @return void
     */
    public function genCode(int $typeMessage, float $value1, float $value2){
        switch ($typeMessage) {
            case 1:
                $code = "aaa.-".rand($value1, $value2);
                break;           
            case 2:
                $code = "bbb.-".rand($value1, $value2);
                break;
            case 3:
                $code = "ccc.-".rand($value1, $value2);
                break;
            default:
                $code = "";
                break;
        }
        return $code;
    }

    /**
     * @Route("/asend", name="asend")
     */
    public function Asend(Request $request,ObjectManager $objectManager, Amessage $amessage)
    {
        if ($request->isXmlHttpRequest()) {
            $message = $request->get('message');
            $destinateur = $request->get('destinateur');
            $destinataire = $request->get('destinataire');
            $data = [
                'emit' => $destinateur,
                'reci' => $destinataire,
                'texto'=> $message
            ];
            $username = $this->getUser()->getUsername();
            $codemsg = genCode(3,0,1000000);

            return $this->json($data);

        }
    }

    /**
     * @Route("/areceive", name="areceive")
     */
    public function Areceive()
    {
        return $this->render('gestionnaire_message/index.html.twig', [
            'controller_name' => 'GestionnaireMessageController',
        ]);
    }

    /**
     * @Route("/bsend", name="bsend")
     */
    public function Bsend(Request $request, ObjectManager $objectManager, Bmessage $bmessage)
    {
        if ($request->isXmlHttpRequest()) {
            $message = $request->get('message');
            $destinateur = $request->get('destinateur');
            $destinataire = $request->get('destinataire');
            $data = [
                'emit' => $destinateur,
                'reci' => $destinataire,
                'texto'=> $message
            ];
            $username = $this->getUser()->getUsername();
            $codemsg = genCode(3,0,1000000);

            return $this->json($data);

        }
    }

    /**
     * @Route("/breceive", name="breceive")
     */
    public function Breceive()
    {
        
    }

    /**
     * @Route("/csend", name="csend")
     */
    public function Csend(Request $request, ObjectManager $objectManager, Cmessage $cmessage)
    {
        if ($request->isXmlHttpRequest()) {
            $message = $request->get('message');
            $destinateur = $request->get('destinateur');
            $destinataire = $request->get('destinataire');
            $data = [
                'emit' => $destinateur,
                'reci' => $destinataire,
                'texto'=> $message
            ];
            $username = $this->getUser()->getUsername();
            $codemsg = genCode(3,0,1000000);

            return $this->json($data);

        }
    }

     /**
     * @Route("/creceive", name="creceive")
     */
    public function Creceive()
    {
        return $this->render('gestionnaire_message/index.html.twig', [
            'controller_name' => 'GestionnaireMessageController',
        ]);
    }

}
