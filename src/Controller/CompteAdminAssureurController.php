<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


use App\Repository\AgentAssureurRepository;
use App\Repository\CompteClientRepository;
use App\Entity\AgentAssureur;
use App\Repository\AdminAssureurRepository;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Entity\Amessage;
use App\Repository\AmessageRepository;
use App\Entity\Bmessage;
use App\Repository\BmessageRepository;
use App\Repository\CompteAssureurRepository;
use App\Entity\Offre;
use App\Repository\OffreRepository;

use App\Entity\CGenerale;
use App\Repository\CGeneraleRepository;

use App\Entity\CParticuliere;
use App\Repository\CParticuliereRepository;


class CompteAdminAssureurController extends AbstractController
{
    /**
     * @Route("/account/admin", name="compte_admin")
     */
    public function index()
    {
        $active = 1;
        
        return $this->render('compte_admin_assureur/index.html.twig', [
            'active' => $active
        ]);
    }

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
      $this->encoder = $encoder;
    }

    /**
     * generateur de code pour les messages
     *
     * @param integer $typeMessage
     * @param float $value1
     * @param float $value2
     * @return void
     */
     public function genCode(int $typeMessage, float $value1, float $value2){
        switch ($typeMessage) {
            case 1:
                $code = "aaa-".rand($value1, $value2);
                break;           
            case 2:
                $code = "bbb-".rand($value1, $value2);
                break;
            case 3:
                $code = "ccc-".rand($value1, $value2);
                break;
            default:
                $code = "";
                break;
        }
        return $code;
    }

    /**
     * @Route("/account/admin/gestion_agent", name="gestion_agent")
     */
    public function gestionAgent(AgentAssureurRepository $agentAssureurRepository)
    {
        $active = 2;

        $agents = $agentAssureurRepository->findAll();
        
        return $this->render('compte_admin_assureur/gestion_agent.html.twig', [
            'active' => $active,
            'agents' => $agents
        ]);
    }

    /**
     * Undocumented function
     * 
     * @Route("/account/admin/gestion_agent/newagent", name="newagent")
     *
     * @param Request $request
     * @param ObjectManager $manager
     * @param User $user
     * @param AdminAssureurRepository $adminAssureurRepository
     * @param AgentAssureur $agentAssureur
     * @return void
     */
    public function ajoutAgent(Request $request,ObjectManager $manager,
                                AdminAssureurRepository $adminAssureurRepository) {
        
    
        $active = 2;

        if ($request->request->count() > 0) {

            $user = new User();
            $agentAssureur = new AgentAssureur();
            
            $nom = $request->request->get('nom');
            $prenom = $request->request->get('prenom');
            $dateregister = new \DateTime();
            $codeuser = "agent-".rand(0, 1200);
            $codeadmin = $this->getUser()->getCodeUser();
            $username = $nom."@gmail.com";
            $status = "agent";
            $password = $this->encoder
                    ->encodePassword($user, "12345678");
            $imma = $adminAssureurRepository->findByCodeAdminAssureur($codeadmin)[0]
                                            ->getCodeAssureur();
            
            $user->setCodeUser($codeuser)
                 ->setNom($nom)
                 ->setPrenom($prenom)
                 ->setusername($username)
                 ->setPassword($password)
                 ->setRoles(["ROLE_AGENT"])
                 ->setStatus($status);
            $agentAssureur->setCodeAdminAssureur($codeadmin)
                          ->setCodeAgentAssureur($codeuser) 
                          ->setNom($nom)
                          ->setPrenom($prenom)
                          ->setImAssureur($imma);
            $manager->persist($agentAssureur);
            $manager->persist($user);
            $manager->flush(); 
            
            return $this->redirectToRoute("gestion_agent");

        }

        return $this->render('compte_admin_assureur/pages/register.html.twig', [
            'active' => $active
        ]);
    }

    /**
     * Undocumented function
     * 
     * @Route("/account/admin/gestion_agent/chat/{code}", name="livechat1")
     *
     * @param [type] $code
     * @param Request $request
     * @return void
     */
    public function suivreAgent($code, Request $request, 
                                ObjectManager $manager ,   
                                UserRepository $userRepository,
                                BmessageRepository $bmessageRepository){

        $active = 2;

        $agent = $userRepository->findByCodeUser($code);
        $bmessages = $bmessageRepository->findAll();
        $destinateur = $this->getUser()->getUsername();
        if (count($agent) > 0) {
            $destinataire = $agent[0]->getUsername();
        }else{
            return $this->redirectToRoute("compte_admin");
        }
        
        if ($request->request->count() > 0) {
            $message = $request->request->get('message');
            
            $bmessage = new Bmessage();
            $codemsg = $this->genCode(2, 0, 2000);

            $bmessage->setCodeMsg($codemsg)
                     ->setDestinateur($destinateur)
                     ->setDestinataire($destinataire)
                     ->setContenu($message);

            $manager->persist($bmessage);
            $manager->flush();

            $bmessages = $bmessageRepository->findAll();

            dump($destinateur,$destinataire,$bmessages);

            return $this->render('compte_admin_assureur/pages/chat.html.twig', [
                'messages' => $bmessages,
                'code' => $code,
                'active' => $active,
                'destinateur' => $destinateur,
                'destinataire' => $destinataire
            ]);

        }

        /*if ($request->isXmlHttpRequest()) {
            $message = $request->get('message');
            $destinateur = $request->get('destinateur');
            $destinataire = $request->get('destinataire');
            $data = [
                'emit' => $destinateur,
                'reci' => $destinataire,
                'texto'=> $message
            ];
            $username = $this->getUser()->getUsername();
            $codemsg = genCode(3,0,1000000);

            return $this->json($data);

        }*/

        dump($bmessages);
       
        return $this->render('compte_admin_assureur/pages/chat.html.twig', [
            'code' => $code,
            'active' => $active,
            'destinateur' => $destinateur,
            'destinataire' => $destinataire,
            'messages' => $bmessages
        ]);
    }

    /**
     * @Route("/account/admin/gestion_client", name="gestion_client")
     */
    public function gestionClient()
    {
        $active = 3;
        
        return $this->render('compte_admin_assureur/gestion_client.html.twig', [
            'active' => $active
        ]);
    }

    /**
     * @Route("/account/admin/root", name="suivre_root")
     */
    public function root(Request $request, 
                        ObjectManager $manager ,   
                        UserRepository $userRepository,
                        AdminAssureurRepository $adminAssureurRepository,
                        CompteAssureurRepository $compteAssureurRepository,
                        AmessageRepository $amessageRepository)
    {
        $active = 4;
        $codeadmin = $this->getUser()->getCodeUser();

        $codeassur = $adminAssureurRepository->findByCodeAdminAssureur($codeadmin)[0]
                                             ->getCodeAssureur();
        $coderoot = $compteAssureurRepository->findByCodeAssureur($codeassur)[0]
        ->getCodeRoot();
        
        $root = $userRepository->findByCodeUser($coderoot);
        $amessages = $amessageRepository->findAll();
        $destinateur = $this->getUser()->getUsername();
        if (count($root) > 0) {
            $destinataire = $root[0]->getUsername();
        }else{
            return $this->redirectToRoute("compte_admin");
        }
        
        if ($request->request->count() > 0) {
            $message = $request->request->get('message');
            
            $amessage = new Amessage();
            $codemsg = $this->genCode(1, 0, 2000);

            $amessage->setCodeMsg($codemsg)
                     ->setDestinateur($destinateur)
                     ->setDestinataire($destinataire)
                     ->setContenu($message);

            $manager->persist($amessage);
            $manager->flush();

            $amessages = $amessageRepository->findAll();

            dump($destinateur,$destinataire,$amessages);

            return $this->render('compte_admin_assureur/root.html.twig', [
                'messages' => $amessages,
                'active' => $active,
                'destinateur' => $destinateur,
                'destinataire' => $destinataire
            ]);

        }
        
        return $this->render('compte_admin_assureur/root.html.twig', [
            'active' => $active,
            'destinateur' => $destinateur,
            'destinataire' => $destinataire,
            'messages' => $amessages
        ]);
    }

    /**
     * @Route("/account/admin/contract", name="infos_contrat")
     */
    public function infosContrat(OffreRepository $offreRepository)
    {
        $active = 5;

        $contrats = $offreRepository->findAll();
        dump($contrats);

        if(count($contrats) > 0){
            return $this->render('compte_admin_assureur/contrat.html.twig', [
                'active' => $active,
                'contrats' => $contrats
            ]);
        }else{
            return $this->render('compte_admin_assureur/contrat2.html.twig', [
                'active' => $active
            ]);
        }
        
        
    }

    /**
     * @Route("/account/admin/notifications", name="notifications_admin")
     */
    public function notifications()
    {
        $active = 6;
        
        return $this->render('compte_admin_assureur/notification.html.twig', [
            'active' => $active
        ]);
    }

    /**
     * @Route("/account/admin/infos", name="infos_admin")
     */
    public function infosAdmin()
    {
        $active = 7;
        $user = $this->getUser();
        
        return $this->render('compte_admin_assureur/infoprofil.html.twig', [
            'active' => $active,
            'user' => $user
        ]);
    }
}
