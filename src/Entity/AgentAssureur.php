<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AgentAssureurRepository")
 */
class AgentAssureur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAdminAssureur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAgentAssureur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imAssureur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $role;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateRegister;

    public function __construct()
    {
        $this->dateRegister = new \DateTime();   
        $this->role = "Service Clientèle";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeAdminAssureur(): ?string
    {
        return $this->codeAdminAssureur;
    }

    public function setCodeAdminAssureur(string $codeAdminAssureur): self
    {
        $this->codeAdminAssureur = $codeAdminAssureur;

        return $this;
    }

    public function getCodeAgentAssureur(): ?string
    {
        return $this->codeAgentAssureur;
    }

    public function setCodeAgentAssureur(string $codeAgentAssureur): self
    {
        $this->codeAgentAssureur = $codeAgentAssureur;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getImAssureur(): ?string
    {
        return $this->imAssureur;
    }

    public function setImAssureur(string $imAssureur): self
    {
        $this->imAssureur = $imAssureur;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getDateRegister(): ?\DateTimeInterface
    {
        return $this->dateRegister;
    }

    public function setDateRegister(\DateTimeInterface $dateRegister): self
    {
        $this->dateRegister = $dateRegister;

        return $this;
    }
}
