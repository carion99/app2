<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompteAssureurRepository")
 */
class CompteAssureur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAssureur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $statutJuridique;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="integer")
     */
    private $phoneGsm;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $norCommerce;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeRoot;

    /**
     * @ORM\Column(type="integer")
     */
    private $permitRoot;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateRegister;

    public function __construct()
    {
        $this->dateRegister = new \DateTime();
        $this->codeRoot = "nothing";
        $this->permitRoot = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeAssureur(): ?string
    {
        return $this->codeAssureur;
    }

    public function setCodeAssureur(string $codeAssureur): self
    {
        $this->codeAssureur = $codeAssureur;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getStatutJuridique(): ?string
    {
        return $this->statutJuridique;
    }

    public function setStatutJuridique(string $statutJuridique): self
    {
        $this->statutJuridique = $statutJuridique;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneGsm(): ?int
    {
        return $this->phoneGsm;
    }

    public function setPhoneGsm(int $phoneGsm): self
    {
        $this->phoneGsm = $phoneGsm;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getNorCommerce(): ?string
    {
        return $this->norCommerce;
    }

    public function setNorCommerce(string $norCommerce): self
    {
        $this->norCommerce = $norCommerce;

        return $this;
    }

    public function getCodeRoot(): ?string
    {
        return $this->codeRoot;
    }

    public function setCodeRoot(string $codeRoot): self
    {
        $this->codeRoot = $codeRoot;

        return $this;
    }

    public function getPermitRoot(): ?int
    {
        return $this->permitRoot;
    }

    public function setPermitRoot(int $permitRoot): self
    {
        $this->permitRoot = $permitRoot;

        return $this;
    }

    public function getDateRegister(): ?\DateTimeInterface
    {
        return $this->dateRegister;
    }

    public function setDateRegister(\DateTimeInterface $dateRegister): self
    {
        $this->dateRegister = $dateRegister;

        return $this;
    }
}
