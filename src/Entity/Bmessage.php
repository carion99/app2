<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BmessageRepository")
 */
class Bmessage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeMsg;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $destinateur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $destinataire;

    /**
     * @ORM\Column(type="text")
     */
    private $contenu;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateregister;

    public function __construct(){
        $this->dateregister = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeMsg(): ?string
    {
        return $this->codeMsg;
    }

    public function setCodeMsg(string $codeMsg): self
    {
        $this->codeMsg = $codeMsg;

        return $this;
    }

    public function getDestinateur(): ?string
    {
        return $this->destinateur;
    }

    public function setDestinateur(string $destinateur): self
    {
        $this->destinateur = $destinateur;

        return $this;
    }

    public function getDestinataire(): ?string
    {
        return $this->destinataire;
    }

    public function setDestinataire(string $destinataire): self
    {
        $this->destinataire = $destinataire;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getDateregister(): ?\DateTimeInterface
    {
        return $this->dateregister;
    }

    public function setDateregister(\DateTimeInterface $dateregister): self
    {
        $this->dateregister = $dateregister;

        return $this;
    }
}
