<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StatAssureurRepository")
 */
class StatAssureur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeStatAssureur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAssureur;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbreAgent;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbreClient;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbreAgentConnect;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbreClientConnect;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeStatAssureur(): ?string
    {
        return $this->codeStatAssureur;
    }

    public function setCodeStatAssureur(string $codeStatAssureur): self
    {
        $this->codeStatAssureur = $codeStatAssureur;

        return $this;
    }

    public function getCodeAssureur(): ?string
    {
        return $this->codeAssureur;
    }

    public function setCodeAssureur(string $codeAssureur): self
    {
        $this->codeAssureur = $codeAssureur;

        return $this;
    }

    public function getNbreAgent(): ?int
    {
        return $this->nbreAgent;
    }

    public function setNbreAgent(int $nbreAgent): self
    {
        $this->nbreAgent = $nbreAgent;

        return $this;
    }

    public function getNbreClient(): ?int
    {
        return $this->nbreClient;
    }

    public function setNbreClient(int $nbreClient): self
    {
        $this->nbreClient = $nbreClient;

        return $this;
    }

    public function getNbreAgentConnect(): ?int
    {
        return $this->nbreAgentConnect;
    }

    public function setNbreAgentConnect(int $nbreAgentConnect): self
    {
        $this->nbreAgentConnect = $nbreAgentConnect;

        return $this;
    }

    public function getNbreClientConnect(): ?int
    {
        return $this->nbreClientConnect;
    }

    public function setNbreClientConnect(int $nbreClientConnect): self
    {
        $this->nbreClientConnect = $nbreClientConnect;

        return $this;
    }
}
