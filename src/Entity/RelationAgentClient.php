<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RelationAgentClientRepository")
 */
class RelationAgentClient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeRelation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAgent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeClient;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateRegister;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeRelation(): ?string
    {
        return $this->codeRelation;
    }

    public function setCodeRelation(string $codeRelation): self
    {
        $this->codeRelation = $codeRelation;

        return $this;
    }

    public function getCodeAgent(): ?string
    {
        return $this->codeAgent;
    }

    public function setCodeAgent(string $codeAgent): self
    {
        $this->codeAgent = $codeAgent;

        return $this;
    }

    public function getCodeClient(): ?string
    {
        return $this->codeClient;
    }

    public function setCodeClient(string $codeClient): self
    {
        $this->codeClient = $codeClient;

        return $this;
    }

    public function getDateRegister(): ?\DateTimeInterface
    {
        return $this->dateRegister;
    }

    public function setDateRegister(\DateTimeInterface $dateRegister): self
    {
        $this->dateRegister = $dateRegister;

        return $this;
    }
}
