<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AvisAssureurRepository")
 */
class AvisAssureur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAvisAssureur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAsureur;

    /**
     * @ORM\Column(type="text")
     */
    private $avis;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateRegister;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeAvisAssureur(): ?string
    {
        return $this->codeAvisAssureur;
    }

    public function setCodeAvisAssureur(string $codeAvisAssureur): self
    {
        $this->codeAvisAssureur = $codeAvisAssureur;

        return $this;
    }

    public function getCodeAsureur(): ?string
    {
        return $this->codeAsureur;
    }

    public function setCodeAsureur(string $codeAsureur): self
    {
        $this->codeAsureur = $codeAsureur;

        return $this;
    }

    public function getAvis(): ?string
    {
        return $this->avis;
    }

    public function setAvis(string $avis): self
    {
        $this->avis = $avis;

        return $this;
    }

    public function getDateRegister(): ?\DateTimeInterface
    {
        return $this->dateRegister;
    }

    public function setDateRegister(\DateTimeInterface $dateRegister): self
    {
        $this->dateRegister = $dateRegister;

        return $this;
    }
}
