<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AttenteValidationRepository")
 */
class AttenteValidation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAv;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $urlRedirect;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $urlValidate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateregister;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeuser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeAv(): ?string
    {
        return $this->codeAv;
    }

    public function setCodeAv(string $codeAv): self
    {
        $this->codeAv = $codeAv;

        return $this;
    }

    public function getUrlRedirect(): ?string
    {
        return $this->urlRedirect;
    }

    public function setUrlRedirect(string $urlRedirect): self
    {
        $this->urlRedirect = $urlRedirect;

        return $this;
    }

    public function getUrlValidate(): ?string
    {
        return $this->urlValidate;
    }

    public function setUrlValidate(string $urlValidate): self
    {
        $this->urlValidate = $urlValidate;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDateregister(): ?\DateTimeInterface
    {
        return $this->dateregister;
    }

    public function setDateregister(\DateTimeInterface $dateregister): self
    {
        $this->dateregister = $dateregister;

        return $this;
    }

    public function getCodeuser(): ?string
    {
        return $this->codeuser;
    }

    public function setCodeuser(string $codeuser): self
    {
        $this->codeuser = $codeuser;

        return $this;
    }
}
