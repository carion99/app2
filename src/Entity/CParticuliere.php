<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CParticuliereRepository")
 */
class CParticuliere
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $profilSousc;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $biensCouvert;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $valeursBiensAssur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $montantPrime;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dateEffetContrat;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $modalitePaiement;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProfilSousc(): ?string
    {
        return $this->profilSousc;
    }

    public function setProfilSousc(string $profilSousc): self
    {
        $this->profilSousc = $profilSousc;

        return $this;
    }

    public function getBiensCouvert(): ?string
    {
        return $this->biensCouvert;
    }

    public function setBiensCouvert(string $biensCouvert): self
    {
        $this->biensCouvert = $biensCouvert;

        return $this;
    }

    public function getValeursBiensAssur(): ?string
    {
        return $this->valeursBiensAssur;
    }

    public function setValeursBiensAssur(string $valeursBiensAssur): self
    {
        $this->valeursBiensAssur = $valeursBiensAssur;

        return $this;
    }

    public function getMontantPrime(): ?string
    {
        return $this->montantPrime;
    }

    public function setMontantPrime(string $montantPrime): self
    {
        $this->montantPrime = $montantPrime;

        return $this;
    }

    public function getDateEffetContrat(): ?string
    {
        return $this->dateEffetContrat;
    }

    public function setDateEffetContrat(string $dateEffetContrat): self
    {
        $this->dateEffetContrat = $dateEffetContrat;

        return $this;
    }

    public function getModalitePaiement(): ?string
    {
        return $this->modalitePaiement;
    }

    public function setModalitePaiement(string $modalitePaiement): self
    {
        $this->modalitePaiement = $modalitePaiement;

        return $this;
    }
}
