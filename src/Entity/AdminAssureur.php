<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdminAssureurRepository")
 */
class AdminAssureur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAdminAssureur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="integer")
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $noCni;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAssureur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $motdepasse;

    public function __construct()
    {
        $this->dateRegister = new \DateTime();
        $this->codeAssureur = "nothing";
        $this->motdepasse = "00001111";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeAdminAssureur(): ?string
    {
        return $this->codeAdminAssureur;
    }

    public function setCodeAdminAssureur(string $codeAdminAssureur): self
    {
        $this->codeAdminAssureur = $codeAdminAssureur;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?int
    {
        return $this->phone;
    }

    public function setPhone(int $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getNoCni(): ?string
    {
        return $this->noCni;
    }

    public function setNoCni(string $noCni): self
    {
        $this->noCni = $noCni;

        return $this;
    }

    public function getCodeAssureur(): ?string
    {
        return $this->codeAssureur;
    }

    public function setCodeAssureur(string $codeAssureur): self
    {
        $this->codeAssureur = $codeAssureur;

        return $this;
    }

    public function getMotdepasse(): ?string
    {
        return $this->motdepasse;
    }

    public function setMotdepasse(string $motdepasse): self
    {
        $this->motdepasse = $motdepasse;

        return $this;
    }
}
