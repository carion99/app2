<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewContratRepository")
 */
class NewContrat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeNewContrat;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intitule;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeModelContrat;

    /**
     * @ORM\Column(type="float")
     */
    private $facteur;

    /**
     * @ORM\Column(type="float")
     */
    private $tva;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $domaine;

    /**
     * @ORM\Column(type="float")
     */
    private $duree;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeNewContrat(): ?string
    {
        return $this->codeNewContrat;
    }

    public function setCodeNewContrat(string $codeNewContrat): self
    {
        $this->codeNewContrat = $codeNewContrat;

        return $this;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getCodeModelContrat(): ?string
    {
        return $this->codeModelContrat;
    }

    public function setCodeModelContrat(string $codeModelContrat): self
    {
        $this->codeModelContrat = $codeModelContrat;

        return $this;
    }

    public function getFacteur(): ?float
    {
        return $this->facteur;
    }

    public function setFacteur(float $facteur): self
    {
        $this->facteur = $facteur;

        return $this;
    }

    public function getTva(): ?float
    {
        return $this->tva;
    }

    public function setTva(float $tva): self
    {
        $this->tva = $tva;

        return $this;
    }

    public function getDomaine(): ?string
    {
        return $this->domaine;
    }

    public function setDomaine(string $domaine): self
    {
        $this->domaine = $domaine;

        return $this;
    }

    public function getDuree(): ?float
    {
        return $this->duree;
    }

    public function setDuree(float $duree): self
    {
        $this->duree = $duree;

        return $this;
    }
}
