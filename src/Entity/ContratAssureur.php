<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContratAssureurRepository")
 */
class ContratAssureur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeContratAssureur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeNewContrat;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAssureur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $signatureRoot;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $signatureAssureur;

    /**
     * @ORM\Column(type="float")
     */
    private $somme;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateRegister;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeContratAssureur(): ?string
    {
        return $this->codeContratAssureur;
    }

    public function setCodeContratAssureur(string $codeContratAssureur): self
    {
        $this->codeContratAssureur = $codeContratAssureur;

        return $this;
    }

    public function getCodeNewContrat(): ?string
    {
        return $this->codeNewContrat;
    }

    public function setCodeNewContrat(string $codeNewContrat): self
    {
        $this->codeNewContrat = $codeNewContrat;

        return $this;
    }

    public function getCodeAssureur(): ?string
    {
        return $this->codeAssureur;
    }

    public function setCodeAssureur(string $codeAssureur): self
    {
        $this->codeAssureur = $codeAssureur;

        return $this;
    }

    public function getSignatureRoot(): ?string
    {
        return $this->signatureRoot;
    }

    public function setSignatureRoot(string $signatureRoot): self
    {
        $this->signatureRoot = $signatureRoot;

        return $this;
    }

    public function getSignatureAssureur(): ?string
    {
        return $this->signatureAssureur;
    }

    public function setSignatureAssureur(string $signatureAssureur): self
    {
        $this->signatureAssureur = $signatureAssureur;

        return $this;
    }

    public function getSomme(): ?float
    {
        return $this->somme;
    }

    public function setSomme(float $somme): self
    {
        $this->somme = $somme;

        return $this;
    }

    public function getDateRegister(): ?\DateTimeInterface
    {
        return $this->dateRegister;
    }

    public function setDateRegister(\DateTimeInterface $dateRegister): self
    {
        $this->dateRegister = $dateRegister;

        return $this;
    }
}
