<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OffreRepository")
 */
class Offre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeOffre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAssureur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intitule;

    /**
     * @ORM\Column(type="float")
     */
    private $cotisation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $avantages;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $domaine;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateRegister;

    /**
     * @ORM\Column(type="float")
     */
    private $duree;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codecg;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codecp;

    /**
     * @ORM\Column(type="float")
     */
    private $montantGarantie;

    public function __construct()
    {
        $this->codeOffre = "offre-".rand(100, 999);
        $this->codecg = "cgenera-".rand(100, 999);
        $this->codecp = "cparti-".rand(100, 999);
        $this->dateRegister = new \DateTime();
        $this->duree = rand(6,36);
        $this->cotisation = rand(20000, 50000);
        $this->montantGarantie = rand(2000000, 50000000);
        $this->avantages = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores 
        molestias doloremque asperiores ipsa a a
        ccusamus ad ullam, modi delectus vero, iure obcaecati laborum repellat,
         iste laboriosam amet. ";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeOffre(): ?string
    {
        return $this->codeOffre;
    }

    public function setCodeOffre(string $codeOffre): self
    {
        $this->codeOffre = $codeOffre;

        return $this;
    }

    public function getCodeAssureur(): ?string
    {
        return $this->codeAssureur;
    }

    public function setCodeAssureur(string $codeAssureur): self
    {
        $this->codeAssureur = $codeAssureur;

        return $this;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getCotisation(): ?float
    {
        return $this->cotisation;
    }

    public function setCotisation(float $cotisation): self
    {
        $this->cotisation = $cotisation;

        return $this;
    }

    public function getAvantages(): ?string
    {
        return $this->avantages;
    }

    public function setAvantages(string $avantages): self
    {
        $this->avantages = $avantages;

        return $this;
    }

    public function getDomaine(): ?string
    {
        return $this->domaine;
    }

    public function setDomaine(string $domaine): self
    {
        $this->domaine = $domaine;

        return $this;
    }

    public function getDateRegister(): ?\DateTimeInterface
    {
        return $this->dateRegister;
    }

    public function setDateRegister(\DateTimeInterface $dateRegister): self
    {
        $this->dateRegister = $dateRegister;

        return $this;
    }

    public function getDuree(): ?float
    {
        return $this->duree;
    }

    public function setDuree(float $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getCodecg(): ?string
    {
        return $this->codecg;
    }

    public function setCodecg(string $codecg): self
    {
        $this->codecg = $codecg;

        return $this;
    }

    public function getCodecp(): ?string
    {
        return $this->codecp;
    }

    public function setCodecp(string $codecp): self
    {
        $this->codecp = $codecp;

        return $this;
    }

    public function getMontantGarantie(): ?float
    {
        return $this->montantGarantie;
    }

    public function setMontantGarantie(float $montantGarantie): self
    {
        $this->montantGarantie = $montantGarantie;

        return $this;
    }
}
