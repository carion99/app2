<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserConnectRepository")
 */
class UserConnect
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeUser;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateConnect;

    /**
     * @ORM\Column(type="float")
     */
    private $timeConnect;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeUser(): ?string
    {
        return $this->codeUser;
    }

    public function setCodeUser(string $codeUser): self
    {
        $this->codeUser = $codeUser;

        return $this;
    }

    public function getDateConnect(): ?\DateTimeInterface
    {
        return $this->dateConnect;
    }

    public function setDateConnect(\DateTimeInterface $dateConnect): self
    {
        $this->dateConnect = $dateConnect;

        return $this;
    }

    public function getTimeConnect(): ?float
    {
        return $this->timeConnect;
    }

    public function setTimeConnect(float $timeConnect): self
    {
        $this->timeConnect = $timeConnect;

        return $this;
    }
}
