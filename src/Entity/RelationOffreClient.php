<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RelationOffreClientRepository")
 */
class RelationOffreClient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codoc;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeClient;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeOffre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAdminAssureur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAssureur;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateRegister;

    public function __construct()
    {
        $this->codoc = "relcof-".rand(1000, 9999);
        $this->dateRegister = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodoc(): ?string
    {
        return $this->codoc;
    }

    public function setCodoc(string $codoc): self
    {
        $this->codoc = $codoc;

        return $this;
    }

    public function getCodeClient(): ?string
    {
        return $this->codeClient;
    }

    public function setCodeClient(string $codeClient): self
    {
        $this->codeClient = $codeClient;

        return $this;
    }

    public function getCodeOffre(): ?string
    {
        return $this->codeOffre;
    }

    public function setCodeOffre(string $codeOffre): self
    {
        $this->codeOffre = $codeOffre;

        return $this;
    }

    public function getCodeAdminAssureur(): ?string
    {
        return $this->codeAdminAssureur;
    }

    public function setCodeAdminAssureur(string $codeAdminAssureur): self
    {
        $this->codeAdminAssureur = $codeAdminAssureur;

        return $this;
    }

    public function getCodeAssureur(): ?string
    {
        return $this->codeAssureur;
    }

    public function setCodeAssureur(string $codeAssureur): self
    {
        $this->codeAssureur = $codeAssureur;

        return $this;
    }

    public function getDateRegister(): ?\DateTimeInterface
    {
        return $this->dateRegister;
    }

    public function setDateRegister(\DateTimeInterface $dateRegister): self
    {
        $this->dateRegister = $dateRegister;

        return $this;
    }
}
