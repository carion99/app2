<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ModelContratRepository")
 */
class ModelContrat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeModelContrat;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intitule;

    /**
     * @ORM\Column(type="float")
     */
    private $nbClient;

    /**
     * @ORM\Column(type="float")
     */
    private $nbAgent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $versionLiveChat;

    /**
     * @ORM\Column(type="float")
     */
    private $capaciteFileBox;

    /**
     * @ORM\Column(type="integer")
     */
    private $permitChatBot;

    /**
     * @ORM\Column(type="integer")
     */
    private $permitChatVideo;

    /**
     * @ORM\Column(type="integer")
     */
    private $permitFileShare;

    /**
     * @ORM\Column(type="integer")
     */
    private $permitAssistance;

    /**
     * @ORM\Column(type="float")
     */
    private $priceHt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeModelContrat(): ?string
    {
        return $this->codeModelContrat;
    }

    public function setCodeModelContrat(string $codeModelContrat): self
    {
        $this->codeModelContrat = $codeModelContrat;

        return $this;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getNbClient(): ?float
    {
        return $this->nbClient;
    }

    public function setNbClient(float $nbClient): self
    {
        $this->nbClient = $nbClient;

        return $this;
    }

    public function getNbAgent(): ?float
    {
        return $this->nbAgent;
    }

    public function setNbAgent(float $nbAgent): self
    {
        $this->nbAgent = $nbAgent;

        return $this;
    }

    public function getVersionLiveChat(): ?string
    {
        return $this->versionLiveChat;
    }

    public function setVersionLiveChat(string $versionLiveChat): self
    {
        $this->versionLiveChat = $versionLiveChat;

        return $this;
    }

    public function getCapaciteFileBox(): ?float
    {
        return $this->capaciteFileBox;
    }

    public function setCapaciteFileBox(float $capaciteFileBox): self
    {
        $this->capaciteFileBox = $capaciteFileBox;

        return $this;
    }

    public function getPermitChatBot(): ?int
    {
        return $this->permitChatBot;
    }

    public function setPermitChatBot(int $permitChatBot): self
    {
        $this->permitChatBot = $permitChatBot;

        return $this;
    }

    public function getPermitChatVideo(): ?int
    {
        return $this->permitChatVideo;
    }

    public function setPermitChatVideo(int $permitChatVideo): self
    {
        $this->permitChatVideo = $permitChatVideo;

        return $this;
    }

    public function getPermitFileShare(): ?int
    {
        return $this->permitFileShare;
    }

    public function setPermitFileShare(int $permitFileShare): self
    {
        $this->permitFileShare = $permitFileShare;

        return $this;
    }

    public function getPermitAssistance(): ?int
    {
        return $this->permitAssistance;
    }

    public function setPermitAssistance(int $permitAssistance): self
    {
        $this->permitAssistance = $permitAssistance;

        return $this;
    }

    public function getPriceHt(): ?float
    {
        return $this->priceHt;
    }

    public function setPriceHt(float $priceHt): self
    {
        $this->priceHt = $priceHt;

        return $this;
    }
}
