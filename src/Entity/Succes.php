<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SuccesRepository")
 */
class Succes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $matricule;

    /**
     * @ORM\Column(type="integer")
     */
    private $etape1;

    /**
     * @ORM\Column(type="integer")
     */
    private $etape2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeAdmin;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateRegister;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    public function setMatricule(string $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getEtape1(): ?int
    {
        return $this->etape1;
    }

    public function setEtape1(int $etape1): self
    {
        $this->etape1 = $etape1;

        return $this;
    }

    public function getEtape2(): ?int
    {
        return $this->etape2;
    }

    public function setEtape2(int $etape2): self
    {
        $this->etape2 = $etape2;

        return $this;
    }

    public function getCodeAdmin(): ?string
    {
        return $this->codeAdmin;
    }

    public function setCodeAdmin(string $codeAdmin): self
    {
        $this->codeAdmin = $codeAdmin;

        return $this;
    }

    public function getDateRegister(): ?\DateTimeInterface
    {
        return $this->dateRegister;
    }

    public function setDateRegister(\DateTimeInterface $dateRegister): self
    {
        $this->dateRegister = $dateRegister;

        return $this;
    }
}
