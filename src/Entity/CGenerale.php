<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CGeneraleRepository")
 */
class CGenerale
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $termes_contrat;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $garantie;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $exclusionGeneale;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $procedureReal;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $procedureDecla;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $recours;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTermesContrat(): ?string
    {
        return $this->termes_contrat;
    }

    public function setTermesContrat(string $termes_contrat): self
    {
        $this->termes_contrat = $termes_contrat;

        return $this;
    }

    public function getGarantie(): ?string
    {
        return $this->garantie;
    }

    public function setGarantie(string $garantie): self
    {
        $this->garantie = $garantie;

        return $this;
    }

    public function getExclusionGeneale(): ?string
    {
        return $this->exclusionGeneale;
    }

    public function setExclusionGeneale(string $exclusionGeneale): self
    {
        $this->exclusionGeneale = $exclusionGeneale;

        return $this;
    }

    public function getProcedureReal(): ?string
    {
        return $this->procedureReal;
    }

    public function setProcedureReal(string $procedureReal): self
    {
        $this->procedureReal = $procedureReal;

        return $this;
    }

    public function getProcedureDecla(): ?string
    {
        return $this->procedureDecla;
    }

    public function setProcedureDecla(string $procedureDecla): self
    {
        $this->procedureDecla = $procedureDecla;

        return $this;
    }

    public function getRecours(): ?string
    {
        return $this->recours;
    }

    public function setRecours(string $recours): self
    {
        $this->recours = $recours;

        return $this;
    }
}
