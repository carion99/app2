<?php

namespace App\Repository;

use App\Entity\ModelContrat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ModelContrat|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModelContrat|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModelContrat[]    findAll()
 * @method ModelContrat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModelContratRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ModelContrat::class);
    }

    // /**
    //  * @return ModelContrat[] Returns an array of ModelContrat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ModelContrat
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
