<?php

namespace App\Repository;

use App\Entity\CompteAssureur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CompteAssureur|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompteAssureur|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompteAssureur[]    findAll()
 * @method CompteAssureur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompteAssureurRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CompteAssureur::class);
    }

    // /**
    //  * @return CompteAssureur[] Returns an array of CompteAssureur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompteAssureur
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
