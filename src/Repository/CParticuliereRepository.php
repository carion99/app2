<?php

namespace App\Repository;

use App\Entity\CParticuliere;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CParticuliere|null find($id, $lockMode = null, $lockVersion = null)
 * @method CParticuliere|null findOneBy(array $criteria, array $orderBy = null)
 * @method CParticuliere[]    findAll()
 * @method CParticuliere[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CParticuliereRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CParticuliere::class);
    }

    // /**
    //  * @return CParticuliere[] Returns an array of CParticuliere objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CParticuliere
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
