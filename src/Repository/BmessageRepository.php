<?php

namespace App\Repository;

use App\Entity\Bmessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Bmessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bmessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bmessage[]    findAll()
 * @method Bmessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BmessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Bmessage::class);
    }

    // /**
    //  * @return Bmessage[] Returns an array of Bmessage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bmessage
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
