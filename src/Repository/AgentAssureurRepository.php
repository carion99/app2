<?php

namespace App\Repository;

use App\Entity\AgentAssureur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AgentAssureur|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgentAssureur|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgentAssureur[]    findAll()
 * @method AgentAssureur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgentAssureurRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AgentAssureur::class);
    }

    // /**
    //  * @return AgentAssureur[] Returns an array of AgentAssureur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AgentAssureur
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
