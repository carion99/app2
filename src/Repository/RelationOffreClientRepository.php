<?php

namespace App\Repository;

use App\Entity\RelationOffreClient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RelationOffreClient|null find($id, $lockMode = null, $lockVersion = null)
 * @method RelationOffreClient|null findOneBy(array $criteria, array $orderBy = null)
 * @method RelationOffreClient[]    findAll()
 * @method RelationOffreClient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RelationOffreClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RelationOffreClient::class);
    }

    // /**
    //  * @return RelationOffreClient[] Returns an array of RelationOffreClient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RelationOffreClient
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
