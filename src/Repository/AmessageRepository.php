<?php

namespace App\Repository;

use App\Entity\Amessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Amessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Amessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Amessage[]    findAll()
 * @method Amessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AmessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Amessage::class);
    }

    // /**
    //  * @return Amessage[] Returns an array of Amessage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Amessage
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
