<?php

namespace App\Repository;

use App\Entity\NewContrat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NewContrat|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewContrat|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewContrat[]    findAll()
 * @method NewContrat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewContratRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NewContrat::class);
    }

    // /**
    //  * @return NewContrat[] Returns an array of NewContrat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NewContrat
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
