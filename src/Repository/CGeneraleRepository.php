<?php

namespace App\Repository;

use App\Entity\CGenerale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CGenerale|null find($id, $lockMode = null, $lockVersion = null)
 * @method CGenerale|null findOneBy(array $criteria, array $orderBy = null)
 * @method CGenerale[]    findAll()
 * @method CGenerale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CGeneraleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CGenerale::class);
    }

    // /**
    //  * @return CGenerale[] Returns an array of CGenerale objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CGenerale
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
