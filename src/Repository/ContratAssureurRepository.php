<?php

namespace App\Repository;

use App\Entity\ContratAssureur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ContratAssureur|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContratAssureur|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContratAssureur[]    findAll()
 * @method ContratAssureur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContratAssureurRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ContratAssureur::class);
    }

    // /**
    //  * @return ContratAssureur[] Returns an array of ContratAssureur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContratAssureur
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
