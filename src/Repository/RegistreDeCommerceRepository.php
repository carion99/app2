<?php

namespace App\Repository;

use App\Entity\RegistreDeCommerce;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RegistreDeCommerce|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegistreDeCommerce|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegistreDeCommerce[]    findAll()
 * @method RegistreDeCommerce[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegistreDeCommerceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RegistreDeCommerce::class);
    }

    // /**
    //  * @return RegistreDeCommerce[] Returns an array of RegistreDeCommerce objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RegistreDeCommerce
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
