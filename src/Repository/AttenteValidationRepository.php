<?php

namespace App\Repository;

use App\Entity\AttenteValidation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AttenteValidation|null find($id, $lockMode = null, $lockVersion = null)
 * @method AttenteValidation|null findOneBy(array $criteria, array $orderBy = null)
 * @method AttenteValidation[]    findAll()
 * @method AttenteValidation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttenteValidationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AttenteValidation::class);
    }

    // /**
    //  * @return AttenteValidation[] Returns an array of AttenteValidation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AttenteValidation
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
