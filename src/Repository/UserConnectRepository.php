<?php

namespace App\Repository;

use App\Entity\UserConnect;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserConnect|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserConnect|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserConnect[]    findAll()
 * @method UserConnect[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserConnectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserConnect::class);
    }

    // /**
    //  * @return UserConnect[] Returns an array of UserConnect objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserConnect
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
