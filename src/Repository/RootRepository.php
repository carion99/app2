<?php

namespace App\Repository;

use App\Entity\Root;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Root|null find($id, $lockMode = null, $lockVersion = null)
 * @method Root|null findOneBy(array $criteria, array $orderBy = null)
 * @method Root[]    findAll()
 * @method Root[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RootRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Root::class);
    }

    // /**
    //  * @return Root[] Returns an array of Root objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Root
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
