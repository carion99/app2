<?php

namespace App\Repository;

use App\Entity\RelationAgentClient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RelationAgentClient|null find($id, $lockMode = null, $lockVersion = null)
 * @method RelationAgentClient|null findOneBy(array $criteria, array $orderBy = null)
 * @method RelationAgentClient[]    findAll()
 * @method RelationAgentClient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RelationAgentClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RelationAgentClient::class);
    }

    // /**
    //  * @return RelationAgentClient[] Returns an array of RelationAgentClient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RelationAgentClient
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
