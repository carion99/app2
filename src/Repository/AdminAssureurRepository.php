<?php

namespace App\Repository;

use App\Entity\AdminAssureur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AdminAssureur|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminAssureur|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminAssureur[]    findAll()
 * @method AdminAssureur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminAssureurRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AdminAssureur::class);
    }

    // /**
    //  * @return AdminAssureur[] Returns an array of AdminAssureur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdminAssureur
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
