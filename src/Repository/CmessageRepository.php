<?php

namespace App\Repository;

use App\Entity\Cmessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Cmessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cmessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cmessage[]    findAll()
 * @method Cmessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CmessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Cmessage::class);
    }

    // /**
    //  * @return Cmessage[] Returns an array of Cmessage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cmessage
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
